package noto.service.upload;

import java.io.IOException;

public interface INOTOUpload {
    public void upload() throws IOException;
}
