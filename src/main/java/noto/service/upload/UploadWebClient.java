package noto.service.upload;

import java.net.URI;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import org.springframework.web.util.DefaultUriBuilderFactory;
import reactor.core.publisher.Mono;

public class UploadWebClient {
	private WebClient client;

	public UploadWebClient() {
		DefaultUriBuilderFactory factory = new DefaultUriBuilderFactory();
		factory.setEncodingMode(DefaultUriBuilderFactory.EncodingMode.URI_COMPONENT);

		client = WebClient
				.builder()
				.uriBuilderFactory(factory)
				.build();
	}

	private <T> Mono<ResponseEntity<String>> getResponseFromNOTO(String notoResourceUrl, String accessToken, T data) {

		try {
			Mono<ResponseEntity<String>> responseEntityMono = client.post()
					.uri(new URI(notoResourceUrl))
					.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
					.headers(h -> h.setBearerAuth(accessToken))
					//.contentType(MediaType.APPLICATION_JSON)
					.body(Mono.just(data), data.getClass())
					.exchange()
					.flatMap(response -> response.toEntity(String.class))
					.flatMap(entity -> {
						return Mono.just(entity);
					});

			return responseEntityMono;

		} catch (Exception ex) {
			System.err.println(ex);
		}

		return null;
	}


	public <T> ResponseEntity<String> getResponseEntity(String notoResourceUrl, String accessToken, T data) {

		return getResponseFromNOTO(notoResourceUrl, accessToken, data).block();
	}
}
