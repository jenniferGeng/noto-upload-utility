package noto.service.upload.model;

public class DebitCheckInitial { //total 13

    private String event_type;
    private String trans_datetime;
    private String timestamp;
    private String id;
    private String identity_key;
    private String merchant_customer_key;
    private String product_id;
    private String merchant_id;
    private String merchant_id_key;
    private String merchant_user_id;
    private String merchant_trans_id;
    private String device_session_id;
    private String trans_originator_ip;

    public DebitCheckInitial(DebitCheckInitialBuilder builder) {
        this.event_type = builder.event_type;
        this.trans_datetime = builder.trans_datetime;
        this.timestamp = builder.timestamp;
        this.id = builder.id;
        this.identity_key = builder.identity_key;
        this.merchant_customer_key = builder.merchant_customer_key;
        this.product_id = builder.product_id;
        this.merchant_id = builder.merchant_id;
        this.merchant_id_key = builder.merchant_id_key;
        this.merchant_user_id = builder.merchant_user_id;
        this.merchant_trans_id = builder.merchant_trans_id;
        this.device_session_id = builder.device_session_id;
        this.trans_originator_ip = builder.trans_originator_ip;
    }

    public static class DebitCheckInitialBuilder {

        private String event_type;
        private String trans_datetime;
        private String timestamp;
        private String id;
        private String identity_key;
        private String merchant_customer_key;
        private String product_id;
        private String merchant_id;
        private String merchant_id_key;
        private String merchant_user_id;
        private String merchant_trans_id;
        private String device_session_id;
        private String trans_originator_ip;

        public DebitCheckInitial build() {
            return new DebitCheckInitial(this);
        }


        public DebitCheckInitialBuilder setEvent_type(String event_type) {
            this.event_type = event_type;
            return this;
        }

        public DebitCheckInitialBuilder setTrans_datetime(String trans_datetime) {
            this.trans_datetime = trans_datetime;
            return this;
        }

        public DebitCheckInitialBuilder setTimestamp(String timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public DebitCheckInitialBuilder setId(String id) {
            this.id = id;
            return this;
        }

        public DebitCheckInitialBuilder setIdentity_key(String identity_key) {
            this.identity_key = identity_key;
            return this;
        }

        public DebitCheckInitialBuilder setMerchant_customer_key(String merchant_customer_key) {
            this.merchant_customer_key = merchant_customer_key;
            return this;
        }

        public DebitCheckInitialBuilder setProduct_id(String product_id) {
            this.product_id = product_id;
            return this;
        }

        public DebitCheckInitialBuilder setMerchant_id(String merchant_id) {
            this.merchant_id = merchant_id;
            return this;
        }

        public DebitCheckInitialBuilder setMerchant_id_key(String merchant_id_key) {
            this.merchant_id_key = merchant_id_key;
            return this;
        }

        public DebitCheckInitialBuilder setMerchant_user_id(String merchant_user_id) {
            this.merchant_user_id = merchant_user_id;
            return this;
        }

        public DebitCheckInitialBuilder setMerchant_trans_id(String merchant_trans_id) {
            this.merchant_trans_id = merchant_trans_id;
            return this;
        }

        public DebitCheckInitialBuilder setDevice_session_id(String device_session_id) {
            this.device_session_id = device_session_id;
            return this;
        }

        public DebitCheckInitialBuilder setTrans_originator_ip(String trans_originator_ip) {
            this.trans_originator_ip = trans_originator_ip;
            return this;
        }
    }

    public String getEvent_type() {
        return event_type;
    }

    public String getTrans_datetime() {
        return trans_datetime;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getId() {
        return id;
    }

    public String getIdentity_key() {
        return identity_key;
    }

    public String getMerchant_customer_key() {
        return merchant_customer_key;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public String getMerchant_id_key() {
        return merchant_id_key;
    }

    public String getMerchant_user_id() {
        return merchant_user_id;
    }

    public String getMerchant_trans_id() {
        return merchant_trans_id;
    }

    public String getDevice_session_id() {
        return device_session_id;
    }

    public String getTrans_originator_ip() {
        return trans_originator_ip;
    }
}
