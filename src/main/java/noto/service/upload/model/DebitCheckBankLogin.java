package noto.service.upload.model;

public class DebitCheckBankLogin {

    private String event_type;
    private String timestamp;
    private String id;
    private String identity_key;
    private String bank_key;
    private String merchant_id;
    private String merchant_id_key;
    private String merchant_user_id;
    private Long trans_accumulator_amount;
    private Long trans_accumulator_amount_converted;
    private String trans_accumulator_amount_currency;
    private String cust_last_name;
    private String login_datetime;
    private String login_balance;
    private String login_balance_converted;
    private String login_balance_currency;

    public DebitCheckBankLogin(DebitCheckBankLoginBuilder builder) {
        this.event_type = builder.event_type;
        this.timestamp = builder.timestamp;
        this.id = builder.id;
        this.identity_key = builder.identity_key;
        this.bank_key = builder.bank_key;
        this.merchant_id = builder.merchant_id;
        this.merchant_id_key = builder.merchant_id_key;
        this.merchant_user_id = builder.merchant_user_id;
        this.trans_accumulator_amount = builder.trans_accumulator_amount;
        this.trans_accumulator_amount_converted = builder.trans_accumulator_amount_converted;
        this.trans_accumulator_amount_currency = builder.trans_accumulator_amount_currency;
        this.cust_last_name = builder.cust_last_name;
        this.login_datetime = builder.login_datetime;
        this.login_balance = builder.login_balance;
        this.login_balance_converted = builder.login_balance_converted;
        this.login_balance_currency = builder.login_balance_currency;
    }

    public String getEvent_type() {
        return event_type;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getId() {
        return id;
    }

    public String getIdentity_key() {
        return identity_key;
    }

    public String getBank_key() {
        return bank_key;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public String getMerchant_id_key() {
        return merchant_id_key;
    }

    public String getMerchant_user_id() {
        return merchant_user_id;
    }

    public Long getTrans_accumulator_amount() {
        return trans_accumulator_amount;
    }

    public Long getTrans_accumulator_amount_converted() {
        return trans_accumulator_amount_converted;
    }

    public String getTrans_accumulator_amount_currency() {
        return trans_accumulator_amount_currency;
    }

    public String getCust_last_name() {
        return cust_last_name;
    }

    public String getLogin_datetime() {
        return login_datetime;
    }

    public String getLogin_balance() {
        return login_balance;
    }

    public String getLogin_balance_converted() {
        return login_balance_converted;
    }

    public String getLogin_balance_currency() {
        return login_balance_currency;
    }

    public static class DebitCheckBankLoginBuilder {

        private String event_type;
        private String timestamp;
        private String id;
        private String identity_key;
        private String bank_key;
        private String merchant_id;
        private String merchant_id_key;
        private String merchant_user_id;
        private Long trans_accumulator_amount;
        private Long trans_accumulator_amount_converted;
        private String trans_accumulator_amount_currency;
        private String cust_last_name;
        private String login_datetime;
        private String login_balance;
        private String login_balance_converted;
        private String login_balance_currency;

        public DebitCheckBankLogin build() {
            return new DebitCheckBankLogin(this);
        }

        public DebitCheckBankLoginBuilder setEvent_type(String event_type) {
            this.event_type = event_type;
            return this;
        }

        public DebitCheckBankLoginBuilder setTimestamp(String timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public DebitCheckBankLoginBuilder setId(String id) {
            this.id = id;
            return this;
        }

        public DebitCheckBankLoginBuilder setIdentity_key(String identity_key) {
            this.identity_key = identity_key;
            return this;
        }

        public DebitCheckBankLoginBuilder setBank_key(String bank_key) {
            this.bank_key = bank_key;
            return this;
        }

        public DebitCheckBankLoginBuilder setMerchant_id(String merchant_id) {
            this.merchant_id = merchant_id;
            return this;
        }

        public DebitCheckBankLoginBuilder setMerchant_id_key(String merchant_id_key) {
            this.merchant_id_key = merchant_id_key;
            return this;
        }

        public DebitCheckBankLoginBuilder setMerchant_user_id(String merchant_user_id) {
            this.merchant_user_id = merchant_user_id;
            return this;
        }

        public DebitCheckBankLoginBuilder setTrans_accumulator_amount(Long trans_accumulator_amount) {
            this.trans_accumulator_amount = trans_accumulator_amount;
            return this;
        }

        public DebitCheckBankLoginBuilder setTrans_accumulator_amount_converted(Long trans_accumulator_amount_converted) {
            this.trans_accumulator_amount_converted = trans_accumulator_amount_converted;
            return this;
        }

        public DebitCheckBankLoginBuilder setTrans_accumulator_amount_currency(String trans_accumulator_amount_currency) {
            this.trans_accumulator_amount_currency = trans_accumulator_amount_currency;
            return this;
        }

        public DebitCheckBankLoginBuilder setCust_last_name(String cust_last_name) {
            this.cust_last_name = cust_last_name;
            return this;
        }

        public DebitCheckBankLoginBuilder setLogin_datetime(String login_datetime) {
            this.login_datetime = login_datetime;
            return this;
        }

        public DebitCheckBankLoginBuilder setLogin_balance(String login_balance) {
            this.login_balance = login_balance;
            return this;
        }

        public DebitCheckBankLoginBuilder setLogin_balance_converted(String login_balance_converted) {
            this.login_balance_converted = login_balance_converted;
            return this;
        }

        public DebitCheckBankLoginBuilder setLogin_balance_currency(String login_balance_currency) {
            this.login_balance_currency = login_balance_currency;
            return this;
        }
    }
}
