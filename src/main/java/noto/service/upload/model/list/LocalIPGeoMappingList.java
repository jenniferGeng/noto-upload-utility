package noto.service.upload.model.list;

public class LocalIPGeoMappingList {
    private String city;
    private String country;
    private String created_datetime;
    private String notes;
    private String state;
    private String $timestamp;
    private String $listItemId;

    public LocalIPGeoMappingList(){}


    public LocalIPGeoMappingList(String city, String country, String created_datetime, String notes, String state){
        this.city = city;
        this.country = country;
        this.created_datetime = created_datetime;
        this.notes = notes;
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCreated_datetime() {
        return created_datetime;
    }

    public void setCreated_datetime(String created_datetime) {
        this.created_datetime = created_datetime;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String get$timestamp() {
        return $timestamp;
    }

    public void set$timestamp(String $timestamp) {
        this.$timestamp = $timestamp;
    }

    public String get$listItemId() {
        return $listItemId;
    }

    public void set$listItemId(String $listItemId) {
        this.$listItemId = $listItemId;
    }
}
