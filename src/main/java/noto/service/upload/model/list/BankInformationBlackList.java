package noto.service.upload.model.list;

public class BankInformationBlackList {
    private String $listItemId;

    private String event_type;
    private String cust_country_code;
    private String bank_fi_account;
    private String bank_fi_account_type;
    private String bank_fi_routing;
    private String merchant_customer_key;
    private String created_datetime;

    private String product_id;
    private String merchant_id;
    private String transaction_id;
    private String reason_type;
    private String reason_code;
    private String last_seen_datetime;
    private String $timestamp;

    private BankInformationBlackList(BankInformationBlackListBuilder builder) {
        this.$listItemId = builder.$listItemId;
        this.event_type = builder.event_type;
        this.bank_fi_account = builder.bank_fi_account;
        this.bank_fi_account_type = builder.bank_fi_account_type;
        this.bank_fi_routing = builder.bank_fi_routing;
        this.merchant_customer_key = builder.merchant_customer_key;
        this.created_datetime = builder.created_datetime;
        this.cust_country_code = builder.cust_country_code;
        this.product_id = builder.product_id;
        this.merchant_id = builder.merchant_id;
        this.transaction_id = builder.transaction_id;
        this.reason_code = builder.reason_code;
        this.reason_type = builder.reason_type;
        this.last_seen_datetime = builder.last_seen_datetime;
        this.$timestamp = builder.$timestamp;

    }

    public String get$listItemId() {
        return $listItemId;
    }

    public String getEvent_type() {
        return event_type;
    }

    public String getBank_fi_account() {
        return bank_fi_account;
    }

    public String getBank_fi_account_type() {
        return bank_fi_account_type;
    }

    public String getBank_fi_routing() {
        return bank_fi_routing;
    }

    public String getMerchant_customer_key() {
        return merchant_customer_key;
    }

    public String getCreated_datetime() {
        return created_datetime;
    }

    public String getCust_country_code() {
        return cust_country_code;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public String getReason_type() {
        return reason_type;
    }

    public String getReason_code() {
        return reason_code;
    }

    public String getLast_seen_datetime() {
        return last_seen_datetime;
    }

    public String get$timestamp() {
        return $timestamp;
    }


    public static class BankInformationBlackListBuilder {
        private String $listItemId;

        private String event_type;
        private String bank_fi_account;
        private String bank_fi_account_type;
        private String bank_fi_routing;
        private String merchant_customer_key;
        private String created_datetime;
        private String cust_country_code;
        private String product_id;
        private String merchant_id;
        private String transaction_id;
        private String reason_type;
        private String reason_code;
        private String last_seen_datetime;
        private String $timestamp;

        public BankInformationBlackListBuilder() {
        }

        public BankInformationBlackList build() {
            return new BankInformationBlackList(this);
        }

        public BankInformationBlackListBuilder set$listItemId(String $listItemId) {
            this.$listItemId = $listItemId;
            return this;
        }

        public BankInformationBlackListBuilder setEvent_type(String event_type) {
            this.event_type = event_type;
            return this;
        }

        public BankInformationBlackListBuilder setBank_fi_account(String bank_fi_account) {
            this.bank_fi_account = bank_fi_account;
            return this;
        }

        public BankInformationBlackListBuilder setBank_fi_account_type(String bank_fi_account_type) {
            this.bank_fi_account_type = bank_fi_account_type;
            return this;
        }

        public BankInformationBlackListBuilder setBank_fi_routing(String bank_fi_routing) {
            this.bank_fi_routing = bank_fi_routing;
            return this;
        }

        public BankInformationBlackListBuilder setMerchant_customer_key(String merchant_customer_key) {
            this.merchant_customer_key = merchant_customer_key;
            return this;
        }

        public BankInformationBlackListBuilder setCreated_datetime(String created_datetime) {
            this.created_datetime = created_datetime;
            return this;
        }

        public BankInformationBlackListBuilder setCust_country_code(String cust_country_code) {
            this.cust_country_code = cust_country_code;
            return this;
        }

        public BankInformationBlackListBuilder setProduct_id(String product_id) {
            this.product_id = product_id;
            return this;
        }

        public BankInformationBlackListBuilder setMerchant_id(String merchant_id) {
            this.merchant_id = merchant_id;
            return this;
        }

        public BankInformationBlackListBuilder setTransaction_id(String transaction_id) {
            this.transaction_id = transaction_id;
            return this;
        }

        public BankInformationBlackListBuilder setReason_type(String reason_type) {
            this.reason_type = reason_type;
            return this;
        }

        public BankInformationBlackListBuilder setReason_code(String reason_code) {
            this.reason_code = reason_code;
            return this;
        }

        public BankInformationBlackListBuilder setLast_seen_datetime(String last_seen_datetime) {
            this.last_seen_datetime = last_seen_datetime;
            return this;
        }

        public BankInformationBlackListBuilder set$timestamp(String $timestamp) {
            this.$timestamp = $timestamp;
            return this;
        }
    }
}
