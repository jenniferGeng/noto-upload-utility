package noto.service.upload.model.list;

public class IdentityBlacklist {

    private String event_type;
    private String merchant_customer_key;
    private String cust_last_name;
    private String cust_zip_code;
    private String cust_date_of_birth;
    private String cust_country_code;
    private String cust_government_id;
    private String cust_government_id_type;
    private String cust_government_id_state;
    private String cust_phone_number;
    private String product_id;
    private String merchant_id;
    private String transaction_id;
    private String reason_type;
    private String reason_code;
    private String last_seen_datetime;
    private String created_datetime;
    private String $timestamp;
    private String $listItemId;

    private IdentityBlacklist(IdentityBlacklistBuilder identityBlacklistBuilder) {
        this.event_type = identityBlacklistBuilder.event_type;
        this.merchant_customer_key = identityBlacklistBuilder.merchant_customer_key;
        this.cust_last_name = identityBlacklistBuilder.cust_last_name;
        this.cust_zip_code = identityBlacklistBuilder.cust_zip_code;
        this.cust_date_of_birth = identityBlacklistBuilder.cust_date_of_birth;
        this.cust_country_code = identityBlacklistBuilder.cust_country_code;
        this.cust_government_id = identityBlacklistBuilder.cust_government_id;
        this.cust_government_id_type = identityBlacklistBuilder.cust_government_id_type;
        this.cust_government_id_state = identityBlacklistBuilder.cust_government_id_state;
        this.cust_phone_number = identityBlacklistBuilder.cust_phone_number;
        this.product_id = identityBlacklistBuilder.product_id;
        this.merchant_id = identityBlacklistBuilder.merchant_id;
        this.transaction_id = identityBlacklistBuilder.transaction_id;
        this.reason_type = identityBlacklistBuilder.reason_type;
        this.reason_code = identityBlacklistBuilder.reason_code;
        this.last_seen_datetime = identityBlacklistBuilder.last_seen_datetime;
        this.created_datetime = identityBlacklistBuilder.created_datetime;
        this.$timestamp = identityBlacklistBuilder.$timestamp;
        this.$listItemId = identityBlacklistBuilder.$listItemId;
    }


    public static class IdentityBlacklistBuilder {

        private String event_type;
        private String merchant_customer_key;
        private String cust_last_name;
        private String cust_zip_code;
        private String cust_date_of_birth;
        private String cust_country_code;
        private String cust_government_id;
        private String cust_government_id_type;
        private String cust_government_id_state;
        private String cust_phone_number;
        private String product_id;
        private String merchant_id;
        private String transaction_id;
        private String reason_type;
        private String reason_code;
        private String last_seen_datetime;
        private String created_datetime;
        private String $timestamp;
        private String $listItemId;

        public IdentityBlacklistBuilder() {
        }

        public IdentityBlacklist build() {
            return new IdentityBlacklist(this);
        }

        //following are properties for IdentityBlacklistBuilder
        public IdentityBlacklistBuilder setEvent_type(String event_type) {
            this.event_type = event_type;
            return this;
        }

        public IdentityBlacklistBuilder setMerchant_customer_key(String merchant_customer_key) {
            this.merchant_customer_key = merchant_customer_key;
            return this;
        }

        public IdentityBlacklistBuilder setCust_last_name(String cust_last_name) {
            this.cust_last_name = cust_last_name;
            return this;
        }

        public IdentityBlacklistBuilder setCust_zip_code(String cust_zip_code) {
            this.cust_zip_code = cust_zip_code;
            return this;
        }

        public IdentityBlacklistBuilder setCust_date_of_birth(String cust_date_of_birth) {
            this.cust_date_of_birth = cust_date_of_birth;
            return this;
        }

        public IdentityBlacklistBuilder setCust_country_code(String cust_country_code) {
            this.cust_country_code = cust_country_code;
            return this;
        }

        public IdentityBlacklistBuilder setCust_government_id(String cust_government_id) {
            this.cust_government_id = cust_government_id;
            return this;
        }

        public IdentityBlacklistBuilder setCust_government_id_type(String cust_government_id_type) {
            this.cust_government_id_type = cust_government_id_type;
            return this;
        }

        public IdentityBlacklistBuilder setCust_government_id_state(String cust_government_id_state) {
            this.cust_government_id_state = cust_government_id_state;
            return this;
        }

        public IdentityBlacklistBuilder setCust_phone_number(String cust_phone_number) {
            this.cust_phone_number = cust_phone_number;
            return this;
        }

        public IdentityBlacklistBuilder setProduct_id(String product_id) {
            this.product_id = product_id;
            return this;
        }

        public IdentityBlacklistBuilder setMerchant_id(String merchant_id) {
            this.merchant_id = merchant_id;
            return this;
        }

        public IdentityBlacklistBuilder setTransaction_id(String transaction_id) {
            this.transaction_id = transaction_id;
            return this;
        }

        public IdentityBlacklistBuilder setReason_type(String reason_type) {
            this.reason_type = reason_type;
            return this;
        }

        public IdentityBlacklistBuilder setReason_code(String reason_code) {
            this.reason_code = reason_code;
            return this;
        }

        public IdentityBlacklistBuilder setLast_seen_datetime(String last_seen_datetime) {
            this.last_seen_datetime = last_seen_datetime;
            return this;
        }

        public IdentityBlacklistBuilder setCreated_datetime(String created_datetime) {
            this.created_datetime = created_datetime;
            return this;
        }

        public IdentityBlacklistBuilder set$Timestamp(String $timestamp) {
            this.$timestamp = $timestamp;
            return this;
        }

        public IdentityBlacklistBuilder set$listItemId(String $listItemId) {
            this.$listItemId = $listItemId;
            return this;
        }
    }

    //following are properties for IdentityBlacklist
    public String get$listItemId() {
        return $listItemId;
    }

    public String getEvent_type() {
        return event_type;
    }

    public String getMerchant_customer_key() {
        return merchant_customer_key;
    }

    public String getCust_last_name() {
        return cust_last_name;
    }

    public String getCust_zip_code() {
        return cust_zip_code;
    }

    public String getCust_date_of_birth() {
        return cust_date_of_birth;
    }

    public String getCust_country_code() {
        return cust_country_code;
    }
    
    public String getCust_government_id() {
        return cust_government_id;
    }

    public String getCust_government_id_type() {
        return cust_government_id_type;
    }

    public String getCust_government_id_state() {
        return cust_government_id_state;
    }

    public String getCust_phone_number() {
        return cust_phone_number;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public String getReason_type() {
        return reason_type;
    }

    public String getReason_code() {
        return reason_code;
    }

    public String getLast_seen_datetime() {
        return last_seen_datetime;
    }

    public String getCreated_datetime() {
        return created_datetime;
    }

    public String get$Timestamp() {
        return $timestamp;
    }
}
