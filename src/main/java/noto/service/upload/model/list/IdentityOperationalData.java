package noto.service.upload.model.list;

public class IdentityOperationalData {

    private String product_id;
    private String first_seen_datetime;
    private String last_seen_datetime;

    public IdentityOperationalData() {
    }

    public IdentityOperationalData(String productId, String firstSeenDatetime, String lastSeenDatetime) {
        this.product_id = productId;
        this.first_seen_datetime = firstSeenDatetime;
        this.last_seen_datetime = lastSeenDatetime;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getFirst_seen_datetime() {
        return first_seen_datetime;
    }

    public void setFirst_seen_datetime(String first_seen_datetime) {
        this.first_seen_datetime = first_seen_datetime;
    }

    public String getLast_seen_datetime() {
        return last_seen_datetime;
    }

    public void setLast_seen_datetime(String last_seen_datetime) {
        this.last_seen_datetime = last_seen_datetime;
    }

}
