package noto.service.upload.model.list;

public class MerchantVIPList {
    private String event_type;
    private String $listItemId;
    private String product_id;
    private String merchant_id;
    private String merchant_user_id;
    private String risk_rating;
    private String status;
    private String admin_id;
    private String last_seen_datetime;
    private String created_datetime;
    private String $timestamp;


    public MerchantVIPList(MerchantVIPListBuilder builder) {
        this.event_type = builder.event_type;
        this.$listItemId = builder.$listItemId;
        this.product_id = builder.product_id;
        this.merchant_id = builder.merchant_id;
        this.merchant_user_id = builder.merchant_user_id;
        this.risk_rating = builder.risk_rating;
        this.status = builder.status;
        this.admin_id = builder.admin_id;
        this.last_seen_datetime = builder.last_seen_datetime;
        this.created_datetime = builder.created_datetime;
        this.$timestamp = builder.$timestamp;
    }

    public String getEvent_type() {
        return event_type;
    }

    public String get$listItemId() {
        return $listItemId;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public String getMerchant_user_id() {
        return merchant_user_id;
    }

    public String getRisk_rating() {
        return risk_rating;
    }

    public String getStatus() {
        return status;
    }

    public String getAdmin_id() {
        return admin_id;
    }

    public String getLast_seen_datetime() {
        return last_seen_datetime;
    }

    public String getCreated_datetime() {
        return created_datetime;
    }

    public String get$timestamp() {
        return $timestamp;
    }

    public static class MerchantVIPListBuilder {
        private String event_type;
        private String $listItemId;
        private String product_id;
        private String merchant_id;
        private String merchant_user_id;
        private String risk_rating;
        private String status;
        private String admin_id;
        private String last_seen_datetime;
        private String created_datetime;
        private String $timestamp;

        public MerchantVIPList build() {
            return new MerchantVIPList(this);
        }

        public MerchantVIPListBuilder setEvent_type(String event_type) {
            this.event_type = event_type;
            return this;
        }

        public MerchantVIPListBuilder set$listItemId(String $listItemId) {
            this.$listItemId = $listItemId;
            return this;
        }

        public MerchantVIPListBuilder setProduct_id(String product_id) {
            this.product_id = product_id;
            return this;
        }

        public MerchantVIPListBuilder setMerchant_id(String merchant_id) {
            this.merchant_id = merchant_id;
            return this;
        }

        public MerchantVIPListBuilder setMerchant_user_id(String merchant_user_id) {
            this.merchant_user_id = merchant_user_id;
            return this;
        }

        public MerchantVIPListBuilder setRisk_rating(String risk_rating) {
            this.risk_rating = risk_rating;
            return this;
        }

        public MerchantVIPListBuilder setStatus(String status) {
            this.status = status;
            return this;
        }

        public MerchantVIPListBuilder setAdmin_id(String admin_id) {
            this.admin_id = admin_id;
            return this;
        }

        public MerchantVIPListBuilder setLast_seen_datetime(String last_seen_datetime) {
            this.last_seen_datetime = last_seen_datetime;
            return this;
        }

        public MerchantVIPListBuilder setCreated_datetime(String created_datetime) {
            this.created_datetime = created_datetime;
            return this;
        }

        public MerchantVIPListBuilder set$timestamp(String $timestamp) {
            this.$timestamp = $timestamp;
            return this;
        }
    }
}
