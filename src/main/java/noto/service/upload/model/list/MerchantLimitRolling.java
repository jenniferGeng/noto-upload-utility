package noto.service.upload.model.list;

public class MerchantLimitRolling {
    private String $listItemId;
    private String event_type;
    private String product_id;
    private String merchant_id;
    private String risk_rating;
    private String rolling_period_in_days;
    private String threshold_amount;
    private String threshold_count;
    private String limit_level_amount;
    private String limit_level_count;
    private String is_vip_risk_rating;
    private String is_enabled;
    private String admin_id;
    private String created_datetime;
    private String $timestamp;

    private MerchantLimitRolling(MerchantLimitRollingBuilder builder) {
        this.$listItemId = builder.$listItemId;
        this.event_type = builder.event_type;
        this.admin_id = builder.admin_id;
        this.created_datetime = builder.created_datetime;
        this.is_enabled = builder.is_enabled;
        this.is_vip_risk_rating = builder.is_vip_risk_rating;
        this.limit_level_amount = builder.limit_level_amount;
        this.limit_level_count = builder.limit_level_count;
        this.merchant_id = builder.merchant_id;
        this.product_id = builder.product_id;
        this.risk_rating = builder.risk_rating;
        this.rolling_period_in_days = builder.rolling_period_in_days;
        this.threshold_amount = builder.threshold_amount;
        this.threshold_count = builder.threshold_count;
        this.$timestamp = builder.$timestamp;
    }

    public static class MerchantLimitRollingBuilder {
        private String $listItemId;
        private String event_type;
        private String product_id;
        private String merchant_id;
        private String risk_rating;
        private String rolling_period_in_days;
        private String threshold_amount;
        private String threshold_count;
        private String limit_level_amount;
        private String limit_level_count;
        private String is_vip_risk_rating;
        private String is_enabled;
        private String admin_id;
        private String created_datetime;
        private String $timestamp;

        public MerchantLimitRollingBuilder(){}

        public MerchantLimitRolling build() {
            return new MerchantLimitRolling(this);
        }

        public MerchantLimitRollingBuilder set$listItemId(String $listItemId) {
            this.$listItemId = $listItemId;
            return this;
        }

        public MerchantLimitRollingBuilder setEvent_type(String event_type) {
            this.event_type = event_type;
            return this;
        }

        public MerchantLimitRollingBuilder setProduct_id(String product_id) {
            this.product_id = product_id;
            return this;
        }

        public MerchantLimitRollingBuilder setMerchant_id(String merchant_id) {
            this.merchant_id = merchant_id;
            return this;
        }

        public MerchantLimitRollingBuilder setRisk_rating(String risk_rating) {
            this.risk_rating = risk_rating;
            return this;
        }

        public MerchantLimitRollingBuilder setRolling_period_in_days(String rolling_period_in_days) {
            this.rolling_period_in_days = rolling_period_in_days;
            return this;
        }

        public MerchantLimitRollingBuilder setThreshold_amount(String threshold_amount) {
            this.threshold_amount = threshold_amount;
            return this;
        }

        public MerchantLimitRollingBuilder setThreshold_count(String threshold_count) {
            this.threshold_count = threshold_count;
            return this;
        }

        public MerchantLimitRollingBuilder setLimit_level_amount(String limit_level_amount) {
            this.limit_level_amount = limit_level_amount;
            return this;
        }

        public MerchantLimitRollingBuilder setLimit_level_count(String limit_level_count) {
            this.limit_level_count = limit_level_count;
            return this;
        }

        public MerchantLimitRollingBuilder setIs_vip_risk_rating(String is_vip_risk_rating) {
            this.is_vip_risk_rating = is_vip_risk_rating;
            return this;
        }

        public MerchantLimitRollingBuilder setIs_enabled(String is_enabled) {
            this.is_enabled = is_enabled;
            return this;
        }

        public MerchantLimitRollingBuilder setAdmin_id(String admin_id) {
            this.admin_id = admin_id;
            return this;
        }

        public MerchantLimitRollingBuilder setCreated_datetime(String created_datetime) {
            this.created_datetime = created_datetime;
            return this;
        }

        public MerchantLimitRollingBuilder set$timestamp(String $timestamp) {
            this.$timestamp = $timestamp;
            return this;
        }
    }

    public String get$listItemId() {
        return $listItemId;
    }

    public String getEvent_type() {
        return event_type;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public String getRisk_rating() {
        return risk_rating;
    }

    public String getRolling_period_in_days() {
        return rolling_period_in_days;
    }

    public String getThreshold_amount() {
        return threshold_amount;
    }

    public String getThreshold_count() {
        return threshold_count;
    }

    public String getLimit_level_amount() {
        return limit_level_amount;
    }

    public String getLimit_level_count() {
        return limit_level_count;
    }

    public String getIs_vip_risk_rating() {
        return is_vip_risk_rating;
    }

    public String getIs_enabled() {
        return is_enabled;
    }

    public String getAdmin_id() {
        return admin_id;
    }

    public String getCreated_datetime() {
        return created_datetime;
    }

    public String get$timestamp() {
        return $timestamp;
    }
}
