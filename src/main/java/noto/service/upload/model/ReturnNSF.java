package noto.service.upload.model;

public class ReturnNSF {

    private String event_type;
    private String timestamp;
    private String id;
    private String identity_key;
    private String bank_key;
    private String product_id;
    private String merchant_id;
    private String merchant_id_key;
    private String merchant_user_id;
    private String merchant_trans_id;
    private String trans_id;
    private String return_nsf1_date;
    private String return_next_action;
    private String return_next_action_date;

    public ReturnNSF(ReturnNSFBuilder builder) {
        this.event_type = builder.event_type;
        this.timestamp = builder.timestamp;
        this.id = builder.id;
        this.identity_key = builder.identity_key;
        this.bank_key = builder.bank_key;
        this.product_id = builder.product_id;
        this.merchant_id = builder.merchant_id;
        this.merchant_id_key = builder.merchant_id_key;
        this.merchant_user_id = builder.merchant_user_id;
        this.merchant_trans_id = builder.merchant_trans_id;
        this.trans_id = builder.trans_id;
        this.return_nsf1_date = builder.return_nsf1_date;
        this.return_next_action = builder.return_next_action;
        this.return_next_action_date = builder.return_next_action_date;
    }

    public String getEvent_type() {
        return event_type;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getId() {
        return id;
    }

    public String getIdentity_key() {
        return identity_key;
    }

    public String getBank_key() {
        return bank_key;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public String getMerchant_id_key() {
        return merchant_id_key;
    }

    public String getMerchant_user_id() {
        return merchant_user_id;
    }

    public String getMerchant_trans_id() {
        return merchant_trans_id;
    }

    public String getTrans_id() {
        return trans_id;
    }

    public String getReturn_nsf1_date() {
        return return_nsf1_date;
    }

    public String getReturn_next_action() {
        return return_next_action;
    }

    public String getReturn_next_action_date() {
        return return_next_action_date;
    }

    public static class ReturnNSFBuilder {

        private String event_type;
        private String timestamp;
        private String id;
        private String identity_key;
        private String bank_key;
        private String product_id;
        private String merchant_id;
        private String merchant_id_key;
        private String merchant_user_id;
        private String merchant_trans_id;
        private String trans_id;
        private String return_nsf1_date;
        private String return_next_action;
        private String return_next_action_date;

        public ReturnNSF build() {
            return new ReturnNSF(this);
        }

        public ReturnNSFBuilder setEvent_type(String event_type) {
            this.event_type = event_type;
            return this;
        }

        public ReturnNSFBuilder setTimestamp(String timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public ReturnNSFBuilder setId(String id) {
            this.id = id;
            return this;
        }

        public ReturnNSFBuilder setIdentity_key(String identity_key) {
            this.identity_key = identity_key;
            return this;
        }

        public ReturnNSFBuilder setBank_key(String bank_key) {
            this.bank_key = bank_key;
            return this;
        }

        public ReturnNSFBuilder setProduct_id(String product_id) {
            this.product_id = product_id;
            return this;
        }

        public ReturnNSFBuilder setMerchant_id(String merchant_id) {
            this.merchant_id = merchant_id;
            return this;
        }

        public ReturnNSFBuilder setMerchant_id_key(String merchant_id_key) {
            this.merchant_id_key = merchant_id_key;
            return this;
        }

        public ReturnNSFBuilder setMerchant_user_id(String merchant_user_id) {
            this.merchant_user_id = merchant_user_id;
            return this;
        }

        public ReturnNSFBuilder setMerchant_trans_id(String merchant_trans_id) {
            this.merchant_trans_id = merchant_trans_id;
            return this;
        }

        public ReturnNSFBuilder setTrans_id(String trans_id) {
            this.trans_id = trans_id;
            return this;
        }

        public ReturnNSFBuilder setReturn_nsf1_date(String return_nsf1_date) {
            this.return_nsf1_date = return_nsf1_date;
            return this;
        }

        public ReturnNSFBuilder setReturn_next_action(String return_next_action) {
            this.return_next_action = return_next_action;
            return this;
        }

        public ReturnNSFBuilder setReturn_next_action_date(String return_next_action_date) {
            this.return_next_action_date = return_next_action_date;
            return this;
        }
    }
}
