package noto.service.upload.model;

public class DebitTransactionFinal {

    private String event_type;
    private String timestamp;
    private String id;
    private String identity_key;
    private String bank_key;

    private String product_id;
    private String merchant_id;
    private String merchant_id_key;
    private String merchant_user_id;
    private String merchant_trans_id;
    private String trans_id;
    private String trans_type_id;
    private Long trans_amount;
    private Long trans_amount_converted;
    private String trans_currency;
    private String trans_state;
    private String trans_country_code;
    private String trans_datetime;
    private String trans_subtype_id;
    private String trans_status_id;
    private String trans_ext_reference_no;
    private String trans_channel;
    private String trans_error_code;
    private String trans_originator_ip;
    private String trans_payee;
    private String cust_full_name;
    private String cust_first_name;
    private String cust_last_name;
    private String cust_address;
    private String cust_zip_code;
    private String cust_country_code;
    private String cust_identity_type;
    private String cust_phone_number;
    private String cust_date_of_birth;
    private String cust_government_id;
    private String cust_government_id_type;
    private String cust_government_id_state;
    private String cust_driving_license;
    private String cust_driving_license_state;
    private String login_datetime;
    private String login_balance;
    private String login_balance_converted;
    private String login_balance_currency;
    private String bank_fi_routing;
    private String bank_fi_acc;
    private String bank_fi_acc_type;

    //follow fields are not in the file
    private double trans_amount_dollars;
    private String trans_amount_currency;
    private String trans_accumulator_amount_currency;

    public DebitTransactionFinal(DebitTransactionFinalBuilder builder) {
        this.event_type = builder.event_type;
        this.timestamp = builder.timestamp;
        this.id = builder.id;
        this.identity_key = builder.identity_key;
        this.bank_key = builder.bank_key;
        this.product_id = builder.product_id;
        this.merchant_id = builder.merchant_id;
        this.merchant_id_key = builder.merchant_id_key;
        this.merchant_user_id = builder.merchant_user_id;
        this.merchant_trans_id = builder.merchant_trans_id;
        this.trans_id = builder.trans_id;
        this.trans_type_id = builder.trans_type_id;
        this.trans_amount = builder.trans_amount;
        this.trans_amount_converted = builder.trans_amount_converted;
        this.trans_currency = builder.trans_currency;
        this.trans_state = builder.trans_state;
        this.trans_country_code = builder.trans_country_code;
        this.trans_datetime = builder.trans_datetime;
        this.trans_subtype_id = builder.trans_subtype_id;
        this.trans_status_id = builder.trans_status_id;
        this.trans_ext_reference_no = builder.trans_ext_reference_no;
        this.trans_channel = builder.trans_channel;
        this.trans_error_code = builder.trans_error_code;
        this.trans_originator_ip = builder.trans_originator_ip;
        this.trans_payee = builder.trans_payee;
        this.cust_full_name = builder.cust_full_name;
        this.cust_first_name = builder.cust_first_name;
        this.cust_last_name = builder.cust_last_name;
        this.cust_address = builder.cust_address;
        this.cust_zip_code = builder.cust_zip_code;
        this.cust_country_code = builder.cust_country_code;
        this.cust_identity_type = builder.cust_identity_type;
        this.cust_phone_number = builder.cust_phone_number;
        this.cust_date_of_birth = builder.cust_date_of_birth;
        this.cust_government_id = builder.cust_government_id;
        this.cust_government_id_type = builder.cust_government_id_type;
        this.cust_government_id_state = builder.cust_government_id_state;
        this.cust_driving_license = builder.cust_driving_license;
        this.cust_driving_license_state = builder.cust_driving_license_state;
        this.login_datetime = builder.login_datetime;
        this.login_balance = builder.login_balance;
        this.login_balance_converted = builder.login_balance_converted;
        this.login_balance_currency = builder.login_balance_currency;
        this.bank_fi_routing = builder.bank_fi_routing;
        this.bank_fi_acc = builder.bank_fi_acc;
        this.bank_fi_acc_type = builder.bank_fi_acc_type;

        this.trans_amount_dollars = builder.trans_amount_dollars;
        this.trans_amount_currency = builder.trans_amount_currency;
        this.trans_accumulator_amount_currency = builder.trans_accumulator_amount_currency;
    }

    public String getEvent_type() {
        return event_type;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getId() {
        return id;
    }

    public String getIdentity_key() {
        return identity_key;
    }

    public String getBank_key() {
        return bank_key;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public String getMerchant_id_key() {
        return merchant_id_key;
    }

    public String getMerchant_user_id() {
        return merchant_user_id;
    }

    public String getMerchant_trans_id() {
        return merchant_trans_id;
    }

    public String getTrans_id() {
        return trans_id;
    }

    public String getTrans_type_id() {
        return trans_type_id;
    }

    public Long getTrans_amount() {
        return trans_amount;
    }

    public Long getTrans_amount_converted() {
        return trans_amount_converted;
    }

    public String getTrans_currency() {
        return trans_currency;
    }

    public String getTrans_state() {
        return trans_state;
    }

    public String getTrans_country_code() {
        return trans_country_code;
    }

    public String getTrans_datetime() {
        return trans_datetime;
    }

    public String getTrans_subtype_id() {
        return trans_subtype_id;
    }

    public String getTrans_status_id() {
        return trans_status_id;
    }

    public String getTrans_ext_reference_no() {
        return trans_ext_reference_no;
    }

    public String getTrans_channel() {
        return trans_channel;
    }

    public String getTrans_error_code() {
        return trans_error_code;
    }

    public String getTrans_originator_ip() {
        return trans_originator_ip;
    }

    public String getTrans_payee() {
        return trans_payee;
    }

    public String getCust_full_name() {
        return cust_full_name;
    }

    public String getCust_first_name() {
        return cust_first_name;
    }

    public String getCust_last_name() {
        return cust_last_name;
    }

    public String getCust_address() {
        return cust_address;
    }

    public String getCust_zip_code() {
        return cust_zip_code;
    }

    public String getCust_country_code() {
        return cust_country_code;
    }

    public String getCust_identity_type() {
        return cust_identity_type;
    }

    public String getCust_phone_number() {
        return cust_phone_number;
    }

    public String getCust_date_of_birth() {
        return cust_date_of_birth;
    }

    public String getCust_government_id() {
        return cust_government_id;
    }

    public String getCust_government_id_type() {
        return cust_government_id_type;
    }

    public String getCust_government_id_state() {
        return cust_government_id_state;
    }

    public String getCust_driving_license() {
        return cust_driving_license;
    }

    public String getCust_driving_license_state() {
        return cust_driving_license_state;
    }

    public String getLogin_datetime() {
        return login_datetime;
    }

    public String getLogin_balance() {
        return login_balance;
    }

    public String getLogin_balance_converted() {
        return login_balance_converted;
    }

    public String getLogin_balance_currency() {
        return login_balance_currency;
    }

    public String getBank_fi_routing() {
        return bank_fi_routing;
    }

    public String getBank_fi_acc() {
        return bank_fi_acc;
    }

    public String getBank_fi_acc_type() {
        return bank_fi_acc_type;
    }

    public double getTrans_amount_dollars() {
        return trans_amount_dollars;
    }

    public String getTrans_amount_currency() {
        return trans_amount_currency;
    }

    public String getTrans_accumulator_amount_currency() {
        return trans_accumulator_amount_currency;
    }

    public static class DebitTransactionFinalBuilder {

        private String event_type; //total 46
        private String timestamp;
        private String id;
        private String identity_key;
        private String bank_key;
        private String product_id;
        private String merchant_id;
        private String merchant_id_key;
        private String merchant_user_id;
        private String merchant_trans_id;
        private String trans_id;
        private String trans_type_id;
        private Long trans_amount;
        private Long trans_amount_converted;
        private String trans_currency;
        private String trans_state;
        private String trans_country_code;
        private String trans_datetime;
        private String trans_subtype_id;
        private String trans_status_id;
        private String trans_ext_reference_no;
        private String trans_channel;
        private String trans_error_code;
        private String trans_originator_ip;
        private String trans_payee;
        private String cust_full_name;
        private String cust_first_name;
        private String cust_last_name;
        private String cust_address;
        private String cust_zip_code;
        private String cust_country_code;
        private String cust_identity_type;
        private String cust_phone_number;
        private String cust_date_of_birth;
        private String cust_government_id;
        private String cust_government_id_type;
        private String cust_government_id_state;
        private String cust_driving_license;
        private String cust_driving_license_state;
        private String login_datetime;
        private String login_balance;
        private String login_balance_converted;
        private String login_balance_currency;
        private String bank_fi_routing;
        private String bank_fi_acc;
        private String bank_fi_acc_type;

        //follow fields are not in the file
        private double trans_amount_dollars;
        private String trans_amount_currency;
        private String trans_accumulator_amount_currency;

        public DebitTransactionFinal build() {
            return new DebitTransactionFinal(this);
        }

        public DebitTransactionFinalBuilder setEvent_type(String event_type) {
            this.event_type = event_type;
            return this;
        }

        public DebitTransactionFinalBuilder setTimestamp(String timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public DebitTransactionFinalBuilder setId(String id) {
            this.id = id;
            return this;
        }

        public DebitTransactionFinalBuilder setIdentity_key(String identity_key) {
            this.identity_key = identity_key;
            return this;
        }

        public DebitTransactionFinalBuilder setBank_key(String bank_key) {
            this.bank_key = bank_key;
            return this;
        }

        public DebitTransactionFinalBuilder setProduct_id(String product_id) {
            this.product_id = product_id;
            return this;
        }

        public DebitTransactionFinalBuilder setMerchant_id(String merchant_id) {
            this.merchant_id = merchant_id;
            return this;
        }

        public DebitTransactionFinalBuilder setMerchant_id_key(String merchant_id_key) {
            this.merchant_id_key = merchant_id_key;
            return this;
        }

        public DebitTransactionFinalBuilder setMerchant_user_id(String merchant_user_id) {
            this.merchant_user_id = merchant_user_id;
            return this;
        }

        public DebitTransactionFinalBuilder setMerchant_trans_id(String merchant_trans_id) {
            this.merchant_trans_id = merchant_trans_id;
            return this;
        }

        public DebitTransactionFinalBuilder setTrans_id(String trans_id) {
            this.trans_id = trans_id;
            return this;
        }

        public DebitTransactionFinalBuilder setTrans_type_id(String trans_type_id) {
            this.trans_type_id = trans_type_id;
            return this;
        }

        public DebitTransactionFinalBuilder setTrans_amount(Long trans_amount) {
            this.trans_amount = trans_amount;
            return this;
        }

        public DebitTransactionFinalBuilder setTrans_amount_converted(Long trans_amount_converted) {
            this.trans_amount_converted = trans_amount_converted;
            return this;
        }

        public DebitTransactionFinalBuilder setTrans_currency(String trans_currency) {
            this.trans_currency = trans_currency;
            return this;
        }

        public DebitTransactionFinalBuilder setTrans_state(String trans_state) {
            this.trans_state = trans_state;
            return this;
        }

        public DebitTransactionFinalBuilder setTrans_country_code(String trans_country_code) {
            this.trans_country_code = trans_country_code;
            return this;
        }

        public DebitTransactionFinalBuilder setTrans_datetime(String trans_datetime) {
            this.trans_datetime = trans_datetime;
            return this;
        }

        public DebitTransactionFinalBuilder setTrans_subtype_id(String trans_subtype_id) {
            this.trans_subtype_id = trans_subtype_id;
            return this;
        }

        public DebitTransactionFinalBuilder setTrans_status_id(String trans_status_id) {
            this.trans_status_id = trans_status_id;
            return this;
        }

        public DebitTransactionFinalBuilder setTrans_ext_reference_no(String trans_ext_reference_no) {
            this.trans_ext_reference_no = trans_ext_reference_no;
            return this;
        }

        public DebitTransactionFinalBuilder setTrans_channel(String trans_channel) {
            this.trans_channel = trans_channel;
            return this;
        }

        public DebitTransactionFinalBuilder setTrans_error_code(String trans_error_code) {
            this.trans_error_code = trans_error_code;
            return this;
        }

        public DebitTransactionFinalBuilder setTrans_originator_ip(String trans_originator_ip) {
            this.trans_originator_ip = trans_originator_ip;
            return this;
        }

        public DebitTransactionFinalBuilder setTrans_payee(String trans_payee) {
            this.trans_payee = trans_payee;
            return this;
        }

        public DebitTransactionFinalBuilder setCust_full_name(String cust_full_name) {
            this.cust_full_name = cust_full_name;
            return this;
        }

        public DebitTransactionFinalBuilder setCust_first_name(String cust_first_name) {
            this.cust_first_name = cust_first_name;
            return this;
        }

        public DebitTransactionFinalBuilder setCust_last_name(String cust_last_name) {
            this.cust_last_name = cust_last_name;
            return this;
        }

        public DebitTransactionFinalBuilder setCust_address(String cust_address) {
            this.cust_address = cust_address;
            return this;
        }

        public DebitTransactionFinalBuilder setCust_zip_code(String cust_zip_code) {
            this.cust_zip_code = cust_zip_code;
            return this;
        }

        public DebitTransactionFinalBuilder setCust_country_code(String cust_country_code) {
            this.cust_country_code = cust_country_code;
            return this;
        }

        public DebitTransactionFinalBuilder setCust_identity_type(String cust_identity_type) {
            this.cust_identity_type = cust_identity_type;
            return this;
        }

        public DebitTransactionFinalBuilder setCust_phone_number(String cust_phone_number) {
            this.cust_phone_number = cust_phone_number;
            return this;
        }

        public DebitTransactionFinalBuilder setCust_date_of_birth(String cust_date_of_birth) {
            this.cust_date_of_birth = cust_date_of_birth;
            return this;
        }

        public DebitTransactionFinalBuilder setCust_government_id(String cust_government_id) {
            this.cust_government_id = cust_government_id;
            return this;
        }

        public DebitTransactionFinalBuilder setCust_government_id_type(String cust_government_id_type) {
            this.cust_government_id_type = cust_government_id_type;
            return this;
        }

        public DebitTransactionFinalBuilder setCust_government_id_state(String cust_government_id_state) {
            this.cust_government_id_state = cust_government_id_state;
            return this;
        }

        public DebitTransactionFinalBuilder setCust_driving_license(String cust_driving_license) {
            this.cust_driving_license = cust_driving_license;
            return this;
        }

        public DebitTransactionFinalBuilder setCust_driving_license_state(String cust_driving_license_state) {
            this.cust_driving_license_state = cust_driving_license_state;
            return this;
        }

        public DebitTransactionFinalBuilder setLogin_datetime(String login_datetime) {
            this.login_datetime = login_datetime;
            return this;
        }

        public DebitTransactionFinalBuilder setLogin_balance(String login_balance) {
            this.login_balance = login_balance;
            return this;
        }

        public DebitTransactionFinalBuilder setLogin_balance_converted(String login_balance_converted) {
            this.login_balance_converted = login_balance_converted;
            return this;
        }

        public DebitTransactionFinalBuilder setLogin_balance_currency(String login_balance_currency) {
            this.login_balance_currency = login_balance_currency;
            return this;
        }

        public DebitTransactionFinalBuilder setBank_fi_routing(String bank_fi_routing) {
            this.bank_fi_routing = bank_fi_routing;
            return this;
        }

        public DebitTransactionFinalBuilder setBank_fi_acc(String bank_fi_acc) {
            this.bank_fi_acc = bank_fi_acc;
            return this;
        }

        public DebitTransactionFinalBuilder setBank_fi_acc_type(String bank_fi_acc_type) {
            this.bank_fi_acc_type = bank_fi_acc_type;
            return this;
        }

        public DebitTransactionFinalBuilder setTrans_amount_dollars(double trans_amount_dollars) {
            this.trans_amount_dollars = trans_amount_dollars;
            return this;
        }

        public DebitTransactionFinalBuilder setTrans_amount_currency(String trans_amount_currency) {
            this.trans_amount_currency = trans_amount_currency;
            return this;
        }

        public DebitTransactionFinalBuilder setTrans_accumulator_amount_currency(String trans_accumulator_amount_currency) {
            this.trans_accumulator_amount_currency = trans_accumulator_amount_currency;
            return this;
        }
    }
}
