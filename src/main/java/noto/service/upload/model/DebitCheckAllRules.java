package noto.service.upload.model;

public class DebitCheckAllRules {

    private String event_type;
    private String timestamp;
    private String id;
    private String identity_key;
    private String bank_key;
    private String merchant_customer_key;
    private String product_id;
    private String merchant_id;
    private String merchant_id_key;
    private String merchant_user_id;
    private String merchant_trans_id;
    private String trans_id;
    private String trans_type_id;
    private Long trans_amount;
    private Long trans_amount_converted;
    private String trans_currency;
    private String trans_state;
    private String trans_country_code;
    private String trans_originator_ip;
    private String cust_full_name;
    private String cust_first_name;
    private String cust_last_name;
    private String cust_address;
    private String cust_zip_code;
    private String cust_country_code;
    private String cust_identity_type;
    private String cust_phone_number;
    private String cust_date_of_birth;
    private String cust_government_id;
    private String cust_government_id_type;
    private String cust_government_id_state;
    private String cust_driving_license;
    private String cust_driving_license_state;
    private String bank_fi_name;
    private String bank_fi_routing;
    private String bank_fi_acc;
    private String bank_fi_acc_type;
    private String device_session_id;

    //follow fields are not in the file
    private double trans_amount_dollars;
    private String trans_amount_currency;
    private String trans_accumulator_amount_currency;

    public DebitCheckAllRules(DebitCheckAllRulesBuilder builder) {
        this.event_type = builder.event_type;
        this.timestamp = builder.timestamp;
        this.id = builder.id;
        this.identity_key = builder.identity_key;
        this.bank_key = builder.bank_key;
        this.merchant_customer_key = builder.merchant_customer_key;
        this.product_id = builder.product_id;
        this.merchant_id = builder.merchant_id;
        this.merchant_user_id = builder.merchant_user_id;
        this.merchant_trans_id = builder.merchant_trans_id;
        this.trans_id = builder.trans_id;
        this.trans_type_id = builder.trans_type_id;
        this.trans_amount = builder.trans_amount;
        this.trans_amount_converted = builder.trans_amount_converted;
        this.trans_currency = builder.trans_currency;
        this.trans_state = builder.trans_state;
        this.trans_country_code = builder.trans_country_code;
        this.trans_originator_ip = builder.trans_originator_ip;
        this.cust_full_name = builder.cust_full_name;
        this.cust_first_name = builder.cust_first_name;
        this.cust_last_name = builder.cust_last_name;
        this.cust_address = builder.cust_address;
        this.cust_zip_code = builder.cust_zip_code;
        this.cust_country_code = builder.cust_country_code;
        this.cust_identity_type = builder.cust_identity_type;
        this.cust_phone_number = builder.cust_phone_number;
        this.cust_date_of_birth = builder.cust_date_of_birth;
        this.cust_government_id = builder.cust_government_id;
        this.cust_government_id_type = builder.cust_government_id_type;
        this.cust_government_id_state = builder.cust_government_id_state;
        this.cust_driving_license = builder.cust_driving_license;
        this.cust_driving_license_state = builder.cust_driving_license_state;
        this.bank_fi_name = builder.bank_fi_name;
        this.bank_fi_routing = builder.bank_fi_routing;
        this.bank_fi_acc = builder.bank_fi_acc;
        this.bank_fi_acc_type = builder.bank_fi_acc_type;
        this.merchant_id_key = builder.merchant_id_key;
        this.device_session_id = builder.device_session_id;

        this.trans_amount_dollars = builder.trans_amount_dollars;
        this.trans_amount_currency = builder.trans_amount_currency;
        this.trans_accumulator_amount_currency = builder.trans_accumulator_amount_currency;
    }

    public double getTrans_amount_dollars() {
        return trans_amount_dollars;
    }

    public String getTrans_amount_currency() {
        return trans_amount_currency;
    }

    public String getTrans_accumulator_amount_currency() {
        return trans_accumulator_amount_currency;
    }

    public static class DebitCheckAllRulesBuilder {

        private String event_type;
        private String timestamp;
        private String id;
        private String identity_key;
        private String bank_key;
        private String merchant_customer_key;
        private String product_id;
        private String merchant_id;
        private String merchant_id_key;
        private String merchant_user_id;
        private String merchant_trans_id;
        private String trans_id;
        private String trans_type_id;
        private Long trans_amount;
        private Long trans_amount_converted;
        private String trans_currency;
        private String trans_state;
        private String trans_country_code;
        private String trans_originator_ip;
        private String cust_full_name;
        private String cust_first_name;
        private String cust_last_name;
        private String cust_address;
        private String cust_zip_code;
        private String cust_country_code;
        private String cust_identity_type;
        private String cust_phone_number;
        private String cust_date_of_birth;
        private String cust_government_id;
        private String cust_government_id_type;
        private String cust_government_id_state;
        private String cust_driving_license;
        private String cust_driving_license_state;
        private String bank_fi_name;
        private String bank_fi_routing;
        private String bank_fi_acc;
        private String bank_fi_acc_type;
        private String device_session_id;

        //follow fields are not in the file
        private double trans_amount_dollars;
        private String trans_amount_currency;

        private String trans_accumulator_amount_currency;

        public DebitCheckAllRulesBuilder() {
        }

        public DebitCheckAllRules build() {
            return new DebitCheckAllRules(this);
        }

        public DebitCheckAllRulesBuilder setEvent_type(String event_type) {
            this.event_type = event_type;
            return this;
        }

        public DebitCheckAllRulesBuilder setTimestamp(String timestamp) {
            this.timestamp = timestamp;
            return this;
        }


        public DebitCheckAllRulesBuilder setIdentity_key(String identity_key) {
            this.identity_key = identity_key;
            return this;
        }

        public DebitCheckAllRulesBuilder setBank_key(String bank_key) {
            this.bank_key = bank_key;
            return this;
        }

        public DebitCheckAllRulesBuilder setMerchant_customer_key(String merchant_customer_key) {
            this.merchant_customer_key = merchant_customer_key;
            return this;
        }

        public DebitCheckAllRulesBuilder setProduct_id(String product_id) {
            this.product_id = product_id;
            return this;
        }

        public DebitCheckAllRulesBuilder setMerchant_id(String merchant_id) {
            this.merchant_id = merchant_id;
            return this;
        }

        public DebitCheckAllRulesBuilder setMerchant_user_id(String merchant_user_id) {
            this.merchant_user_id = merchant_user_id;
            return this;
        }

        public DebitCheckAllRulesBuilder setMerchant_trans_id(String merchant_trans_id) {
            this.merchant_trans_id = merchant_trans_id;
            return this;
        }

        public DebitCheckAllRulesBuilder setTrans_id(String trans_id) {
            this.trans_id = trans_id;
            return this;
        }

        public DebitCheckAllRulesBuilder setTrans_type_id(String trans_type_id) {
            this.trans_type_id = trans_type_id;
            return this;
        }

        public DebitCheckAllRulesBuilder setTrans_amount(Long trans_amount) {
            this.trans_amount = trans_amount;
            return this;
        }

        public DebitCheckAllRulesBuilder setTrans_amount_converted(Long trans_amount_converted) {
            this.trans_amount_converted = trans_amount_converted;
            return this;
        }

        public DebitCheckAllRulesBuilder setTrans_currency(String trans_currency) {
            this.trans_currency = trans_currency;
            return this;
        }

        public DebitCheckAllRulesBuilder setTrans_state(String trans_state) {
            this.trans_state = trans_state;
            return this;
        }

        public DebitCheckAllRulesBuilder setTrans_country_code(String trans_country_code) {
            this.trans_country_code = trans_country_code;
            return this;
        }

        public DebitCheckAllRulesBuilder setTrans_originator_ip(String trans_originator_ip) {
            this.trans_originator_ip = trans_originator_ip;
            return this;
        }

        public DebitCheckAllRulesBuilder setCust_full_name(String cust_full_name) {
            this.cust_full_name = cust_full_name;
            return this;
        }

        public DebitCheckAllRulesBuilder setCust_first_name(String cust_first_name) {
            this.cust_first_name = cust_first_name;
            return this;
        }

        public DebitCheckAllRulesBuilder setCust_last_name(String cust_last_name) {
            this.cust_last_name = cust_last_name;
            return this;
        }

        public DebitCheckAllRulesBuilder setCust_address(String cust_address) {
            this.cust_address = cust_address;
            return this;
        }

        public DebitCheckAllRulesBuilder setCust_zip_code(String cust_zip_code) {
            this.cust_zip_code = cust_zip_code;
            return this;
        }

        public DebitCheckAllRulesBuilder setCust_country_code(String cust_country_code) {
            this.cust_country_code = cust_country_code;
            return this;
        }

        public DebitCheckAllRulesBuilder setCust_identity_type(String cust_identity_type) {
            this.cust_identity_type = cust_identity_type;
            return this;
        }

        public DebitCheckAllRulesBuilder setCust_phone_number(String cust_phone_number) {
            this.cust_phone_number = cust_phone_number;
            return this;
        }

        public DebitCheckAllRulesBuilder setCust_date_of_birth(String cust_date_of_birth) {
            this.cust_date_of_birth = cust_date_of_birth;
            return this;
        }

        public DebitCheckAllRulesBuilder setCust_government_id(String cust_government_id) {
            this.cust_government_id = cust_government_id;
            return this;
        }

        public DebitCheckAllRulesBuilder setCust_government_id_type(String cust_government_id_type) {
            this.cust_government_id_type = cust_government_id_type;
            return this;
        }

        public DebitCheckAllRulesBuilder setCust_government_id_state(String cust_government_id_state) {
            this.cust_government_id_state = cust_government_id_state;
            return this;
        }

        public DebitCheckAllRulesBuilder setCust_driving_license(String cust_driving_license) {
            this.cust_driving_license = cust_driving_license;
            return this;
        }

        public DebitCheckAllRulesBuilder setCust_driving_license_state(String cust_driving_license_state) {
            this.cust_driving_license_state = cust_driving_license_state;
            return this;
        }

        public DebitCheckAllRulesBuilder setBank_fi_name(String bank_fi_name) {
            this.bank_fi_name = bank_fi_name;
            return this;
        }

        public DebitCheckAllRulesBuilder setBank_fi_routing(String bank_fi_routing) {
            this.bank_fi_routing = bank_fi_routing;
            return this;
        }

        public DebitCheckAllRulesBuilder setBank_fi_acc(String bank_fi_acc) {
            this.bank_fi_acc = bank_fi_acc;
            return this;
        }

        public DebitCheckAllRulesBuilder setBank_fi_acc_type(String bank_fi_acc_type) {
            this.bank_fi_acc_type = bank_fi_acc_type;
            return this;
        }

        public DebitCheckAllRulesBuilder setId(String id) {
            this.id = id;
            return this;
        }

        public DebitCheckAllRulesBuilder setMerchant_id_key(String merchant_id_key) {
            this.merchant_id_key = merchant_id_key;
            return this;
        }

        public DebitCheckAllRulesBuilder setDevice_session_id(String device_session_id) {
            this.device_session_id = device_session_id;
            return this;
        }

        public DebitCheckAllRulesBuilder setTrans_amount_dollars(double trans_amount_dollars) {
            this.trans_amount_dollars = trans_amount_dollars;
            return this;
        }

        public DebitCheckAllRulesBuilder setTrans_amount_currency(String trans_amount_currency) {
            this.trans_amount_currency = trans_amount_currency;
            return this;
        }

        public DebitCheckAllRulesBuilder setTrans_accumulator_amount_currency(String trans_accumulator_amount_currency) {
            this.trans_accumulator_amount_currency = trans_accumulator_amount_currency;
            return this;
        }
    }

    public String getMerchant_id_key() {
        return merchant_id_key;
    }

    public String getDevice_session_id() {
        return device_session_id;
    }

    public String getEvent_type() {
        return event_type;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getId() {
        return id;
    }

    public String getIdentity_key() {
        return identity_key;
    }

    public String getBank_key() {
        return bank_key;
    }

    public String getMerchant_customer_key() {
        return merchant_customer_key;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public String getMerchant_user_id() {
        return merchant_user_id;
    }

    public String getMerchant_trans_id() {
        return merchant_trans_id;
    }

    public String getTrans_id() {
        return trans_id;
    }

    public String getTrans_type_id() {
        return trans_type_id;
    }

    public Long getTrans_amount() {
        return trans_amount;
    }

    public Long getTrans_amount_converted() {
        return trans_amount_converted;
    }

    public String getTrans_currency() {
        return trans_currency;
    }

    public String getTrans_state() {
        return trans_state;
    }

    public String getTrans_country_code() {
        return trans_country_code;
    }

    public String getTrans_originator_ip() {
        return trans_originator_ip;
    }

    public String getCust_full_name() {
        return cust_full_name;
    }

    public String getCust_first_name() {
        return cust_first_name;
    }

    public String getCust_last_name() {
        return cust_last_name;
    }

    public String getCust_address() {
        return cust_address;
    }

    public String getCust_zip_code() {
        return cust_zip_code;
    }

    public String getCust_country_code() {
        return cust_country_code;
    }

    public String getCust_identity_type() {
        return cust_identity_type;
    }

    public String getCust_phone_number() {
        return cust_phone_number;
    }

    public String getCust_date_of_birth() {
        return cust_date_of_birth;
    }

    public String getCust_government_id() {
        return cust_government_id;
    }

    public String getCust_government_id_type() {
        return cust_government_id_type;
    }

    public String getCust_government_id_state() {
        return cust_government_id_state;
    }

    public String getCust_driving_license() {
        return cust_driving_license;
    }

    public String getCust_driving_license_state() {
        return cust_driving_license_state;
    }

    public String getBank_fi_name() {
        return bank_fi_name;
    }

    public String getBank_fi_routing() {
        return bank_fi_routing;
    }

    public String getBank_fi_acc() {
        return bank_fi_acc;
    }

    public String getBank_fi_acc_type() {
        return bank_fi_acc_type;
    }
}
