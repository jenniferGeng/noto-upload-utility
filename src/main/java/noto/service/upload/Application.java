package noto.service.upload;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

public class Application {
    private final static Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        logger.info("Starting.....");
        long startTime = System.nanoTime();

        try {
            INOTOUpload uploadDataList = new UploadTransactions();
            uploadDataList.upload();

        } catch (IOException e) {
            e.printStackTrace();
        }

        long endTime = System.nanoTime();
        // get difference of two nanoTime values
        long timeElapsed = endTime - startTime;

        logger.info("\nExecution time in seconds : " + timeElapsed / 1000000/1000);
    }
}
