package noto.service.upload;

import static noto.service.upload.CommonUtilities.Credit_Check_All_Rules;
import static noto.service.upload.CommonUtilities.Debit_Check_All_Rules;
import static noto.service.upload.CommonUtilities.Debit_Check_Bank_Login;
import static noto.service.upload.CommonUtilities.Debit_Check_Initial;
import static noto.service.upload.CommonUtilities.Debit_Check_Transaction;
import static noto.service.upload.CommonUtilities.Debit_Transaction_Final;
import static noto.service.upload.CommonUtilities.Return_Representment_Transaction_NSF1;
import static noto.service.upload.CommonUtilities.Return_Representment_Transaction_NSF2;
import static noto.service.upload.CommonUtilities.Return_Transaction_Final_Return;
import static noto.service.upload.CommonUtilities.formatAmt;
import static noto.service.upload.CommonUtilities.printFailedRecords;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static noto.service.upload.CommonUtilities.baseUrl;
import static noto.service.upload.LoadProperties.accessCode;
import static noto.service.upload.LoadProperties.eventType;
import static noto.service.upload.LoadProperties.expectedColumnNumber;
import static noto.service.upload.LoadProperties.fileNmae;
import static noto.service.upload.LoadProperties.idColumnIndex;

import java.util.stream.Stream;
import noto.service.upload.model.CreditCheckAllRules;
import noto.service.upload.model.DebitCheckAllRules;
import noto.service.upload.model.DebitCheckBankLogin;
import noto.service.upload.model.DebitCheckInitial;
import noto.service.upload.model.DebitCheckTransaction;
import noto.service.upload.model.DebitTransactionFinal;
import noto.service.upload.model.ReturnNSF;
import noto.service.upload.types.BankAccountTypes;
import noto.service.upload.types.EnumHelper;
import noto.service.upload.types.ProcessingChannelTypes;
import noto.service.upload.types.TransStatus;
import noto.service.upload.types.TransSubTypes;
import noto.service.upload.types.TransTypes;
import org.apache.commons.lang3.text.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;

public class UploadTransactions implements INOTOUpload {

    public static final String baseUri = baseUrl + "/1.0/feed/eCheckSelect/events?division=";
    public static final String notoResourceUrl = baseUri + LoadProperties.divisionFullName;
    public static final String separatorReg = ",(?=([^\"]*\"[^\"]*\")*[^\"]*$)";

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    public UploadWebClient client = new UploadWebClient();

    @Override
    public void upload() throws IOException {

        logger.info(eventType);
        setValues();

        AtomicInteger lineNum = new AtomicInteger();
        AtomicInteger numberOfAdded = new AtomicInteger();
        AtomicInteger numberOfFailed = new AtomicInteger();
        AtomicInteger total = new AtomicInteger();
        List<String> failedRecordList = new ArrayList<>();

        String filePath = new File("").getAbsolutePath() + "/src/main/resources/listFiles/NOTO.Data.RMGDowntime.PROD_20210421/" + fileNmae;
        try (Stream<String> lines = Files.lines(Paths.get(filePath))) {
            lines.forEach(line -> {

                String[] oneLine = CommonUtilities
                        .removeDoubleQuote4Array(line.split(separatorReg, -1)); //second parameter -1 means we want to keep the empty string as part of the array

                lineNum.getAndIncrement();
                if (lineNum.get() > 1) {

                    if (!StringUtils.isEmpty(oneLine)) {
                        total.getAndIncrement();

                        String id = oneLine[idColumnIndex];

                        if (oneLine.length != expectedColumnNumber) {
                            System.err.println(lineNum + ": " + id + " does not have enough data!");
                            numberOfFailed.getAndIncrement();
                            failedRecordList.add(line);

                        } else {
                            try {
                                ResponseEntity<String> resp = getResponseEntity(oneLine, notoResourceUrl);

                                if (resp != null && resp.getStatusCode() == HttpStatus.OK) {
                                    numberOfAdded.getAndIncrement();
                                    logger.info(lineNum + ": " + id + " ---- " + " Upload Successful");

                                } else {
                                    numberOfFailed.getAndIncrement();
                                    System.err.println(
                                            lineNum + ": " + id + " Upload Failed. " + "Status code: " + resp.getStatusCode());
                                    failedRecordList.add(line);
                                }
                            } catch (Exception e) {
                                numberOfFailed.getAndIncrement();
                                System.err.println(
                                        lineNum + ": " + id + " Upload Failed. " + e.getMessage());
                                failedRecordList.add(line);
                                e.printStackTrace();
                            }
                        }

                        try {
                            Thread.sleep(10);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                    } else {
                        logger.info(lineNum + ": is empty line");
                    }
                }
            });
        }

        logger.info("Done! There are total " + total + " items. " + numberOfAdded + " item(s) were added successfully...." + numberOfFailed + " item(s) failed.");

        printFailedRecords(failedRecordList);
    }

    private static void setValues() {
        switch (eventType) {
            case Debit_Check_Initial:
                expectedColumnNumber = 13;
                idColumnIndex = 3;
                break;

            case Debit_Check_Transaction:
                expectedColumnNumber = 48;
                idColumnIndex = 2;
                break;

            case Debit_Transaction_Final:
                expectedColumnNumber = 46;
                idColumnIndex = 2;
                break;

            case Debit_Check_All_Rules:
                expectedColumnNumber = 39;
                idColumnIndex = 2;
                break;

            case Debit_Check_Bank_Login:
                idColumnIndex = 2;
                expectedColumnNumber = 16;
                break;

            case Credit_Check_All_Rules:
                idColumnIndex = 2;
                expectedColumnNumber = 42;
                break;

            case Return_Representment_Transaction_NSF1:

            case Return_Representment_Transaction_NSF2:
                idColumnIndex = 2;
                expectedColumnNumber = 14;
                break;

            case Return_Transaction_Final_Return:
                idColumnIndex = 2;
                expectedColumnNumber = 18;
                break;
            default:
        }
    }

    private ResponseEntity<String> getResponseEntity(String[] oneLine, String notoResourceUrl) {
        ResponseEntity<String> response = null;
        int i = 0;

        switch (eventType) {
            case Debit_Check_Initial:
                DebitCheckInitial debitCheckInitial = new DebitCheckInitial.DebitCheckInitialBuilder()
                        .setEvent_type(oneLine[i++])
                        .setTrans_datetime(oneLine[i++])//1
                        .setTimestamp(oneLine[i++])//2
                        .setId(oneLine[i++])//3
                        .setIdentity_key(oneLine[i++])//4
                        .setMerchant_customer_key(oneLine[i++])//5
                        .setProduct_id(oneLine[i++])//6
                        .setMerchant_id(oneLine[i++])//7
                        .setMerchant_id_key(oneLine[i++]) //8
                        .setMerchant_user_id(oneLine[i++]) //9
                        .setMerchant_trans_id(oneLine[i++])//10
                        .setDevice_session_id(oneLine[i++])//11
                        .setTrans_originator_ip(oneLine[i])//12
                        .build();

                response = client.getResponseEntity(notoResourceUrl, accessCode, debitCheckInitial);
                break;

            case Debit_Check_Transaction:
                DebitCheckTransaction debitCheckTransaction = getDebitCheckTransaction(oneLine);
                response = client.getResponseEntity(notoResourceUrl, accessCode, debitCheckTransaction);
                break;

            case Debit_Transaction_Final:
                DebitTransactionFinal debitTransactionFinal = getDebitTransactionFinal(oneLine);
                response = client.getResponseEntity(notoResourceUrl, accessCode, debitTransactionFinal);
                break;

            case Debit_Check_All_Rules:
                DebitCheckAllRules debitCheckAllRules = getDebitCheckAllRules(oneLine);
                response = client.getResponseEntity(notoResourceUrl, accessCode, debitCheckAllRules);
                break;

            case Debit_Check_Bank_Login:
                DebitCheckBankLogin debitCheckBankLogin = new DebitCheckBankLogin.DebitCheckBankLoginBuilder()
                        .setEvent_type(oneLine[i++]) //0
                        .setTimestamp(oneLine[i++])//1
                        .setId(oneLine[i++])//2
                        .setIdentity_key(oneLine[i++])//3
                        .setBank_key(oneLine[i++])//4
                        .setMerchant_id(oneLine[i++])//5
                        .setMerchant_id_key(oneLine[i++])//6
                        .setMerchant_user_id(oneLine[i++])//7
                        .setTrans_accumulator_amount(formatAmt(oneLine[i++]))//8
                        .setTrans_accumulator_amount_converted(formatAmt(oneLine[i++]))//9
                        .setTrans_accumulator_amount_currency(oneLine[i++])//10
                        .setCust_last_name(oneLine[i++])//11
                        .setLogin_datetime(oneLine[i++])//12
                        .setLogin_balance(oneLine[i++])//13
                        .setLogin_balance_converted(oneLine[i++])//14
                        .setLogin_balance_currency(oneLine[i])//15
                        .build();

                response = client.getResponseEntity(notoResourceUrl, accessCode, debitCheckBankLogin);
                break;

            case Credit_Check_All_Rules:
                CreditCheckAllRules creditCheckAllRules = getCreditCheckAllRules(oneLine);
                response = client.getResponseEntity(notoResourceUrl, accessCode, creditCheckAllRules);
                break;

            case Return_Representment_Transaction_NSF1:

                ReturnNSF returnNSF = new ReturnNSF.ReturnNSFBuilder()
                        .setEvent_type(oneLine[i++]) //0
                        .setTimestamp(oneLine[i++])//1
                        .setId(oneLine[i++])//2
                        .setIdentity_key(oneLine[i++])//3
                        .setBank_key(oneLine[i++])//4
                        .setProduct_id(oneLine[i++])//5
                        .setMerchant_id(oneLine[i++])//6
                        .setMerchant_id_key(oneLine[i++])//7
                        .setMerchant_user_id(oneLine[i++])//8
                        .setMerchant_trans_id(oneLine[i++])//9
                        .setTrans_id(oneLine[i++])//10
                        .setReturn_nsf1_date(oneLine[i++])//11
                        .setReturn_next_action(oneLine[i++])//12
                        .setReturn_next_action_date(oneLine[i])//13
                        .build();
                response = client.getResponseEntity(notoResourceUrl, accessCode, returnNSF);
                break;
            case Return_Representment_Transaction_NSF2:

                break;
            case Return_Transaction_Final_Return:

                break;
            default:

        }
        return response;
    }

    private CreditCheckAllRules getCreditCheckAllRules(String[] oneLine) {
        int i = 0;
        return new CreditCheckAllRules.CreditCheckAllRulesBuilder()
                .setEvent_type(oneLine[i++]) //0
                .setTimestamp(oneLine[i++]) //1
                .setId(oneLine[i++]) //2
                .setIdentity_key(oneLine[i++]) //3
                .setBank_key(oneLine[i++]) //4
                .setMerchant_customer_key(oneLine[i++]) //5
                .setProduct_id(oneLine[i++]) //6
                .setMerchant_id(oneLine[i++]) //7
                .setMerchant_id_key(oneLine[i++]) //8
                .setMerchant_user_id(oneLine[i++])//9
                .setMerchant_trans_id(oneLine[i++])//10
                .setTrans_id(oneLine[i++])//11
                .setTrans_type_id(EnumHelper.from(oneLine[i++], TransTypes.class).name())//12
                .setTrans_amount(formatAmt(oneLine[i++]))//13
                .setTrans_amount_converted(formatAmt(oneLine[i++]))//14
                .setTrans_currency(oneLine[i++])//15
                .setTrans_state(oneLine[i++]) //16
                .setTrans_country_code(oneLine[i++])//17
                .setTrans_subtype_id(EnumHelper.from(oneLine[i++], TransSubTypes.class).name())//18
                .setTrans_status_id(EnumHelper.from(oneLine[i++], TransStatus.class).name())//19
                .setTrans_ext_reference_no(oneLine[i++])//20
                .setTrans_channel(EnumHelper.from(oneLine[i++], ProcessingChannelTypes.class).name())//21
                .setTrans_error_code(oneLine[i++])//22
                .setTrans_originator_ip(oneLine[i++])//23
                .setTrans_payee(oneLine[i++])//24
                .setCust_full_name(oneLine[i++])//25
                .setCust_first_name(oneLine[i++])//26
                .setCust_last_name(oneLine[i++])//27
                .setCust_address(oneLine[i++])//28
                .setCust_zip_code(oneLine[i++])//29
                .setCust_country_code(oneLine[i++])//30
                .setCust_identity_type(WordUtils.capitalize(oneLine[i++].toLowerCase()))//31
                .setCust_phone_number(oneLine[i++])//32
                .setCust_date_of_birth(oneLine[i++])//33
                .setCust_government_id(oneLine[i++])//34
                .setCust_government_id_type(oneLine[i++])//35
                .setCust_government_id_state(oneLine[i++])//36
                .setCust_driving_license(oneLine[i++])//37
                .setCust_driving_license_state(oneLine[i++])//38
                .setBank_fi_routing(oneLine[i++])//39
                .setBank_fi_acc(oneLine[i++])//40
                .setBank_fi_acc_type(EnumHelper.from(oneLine[i], BankAccountTypes.class).name())//41

                .setTrans_amount_dollars(formatAmt(oneLine[13]) / 100.00)
                .setTrans_amount_currency(oneLine[15])
                .setTrans_accumulator_amount_currency(oneLine[15])
                .build();
    }

    private DebitCheckAllRules getDebitCheckAllRules(String[] oneLine) {
        int i = 0;
        return new DebitCheckAllRules.DebitCheckAllRulesBuilder()
                .setEvent_type(oneLine[i++]) //0
                .setTimestamp(oneLine[i++])//1
                .setId(oneLine[i++])//2
                .setIdentity_key(oneLine[i++])//3
                .setBank_key(oneLine[i++])//4
                .setMerchant_customer_key(oneLine[i++])//5
                .setProduct_id(oneLine[i++])//6
                .setMerchant_id(oneLine[i++])//7
                .setMerchant_id_key(oneLine[i++])//8
                .setMerchant_user_id(oneLine[i++])//9
                .setMerchant_trans_id(oneLine[i++])//10
                .setTrans_id(oneLine[i++])//11
                .setTrans_type_id(EnumHelper.from(oneLine[i++], TransTypes.class).name())//12
                .setTrans_amount(formatAmt(oneLine[i++]))//13
                .setTrans_amount_converted(formatAmt(oneLine[i++]))//14
                .setTrans_currency(oneLine[i++])//15
                .setTrans_state(oneLine[i++]) //16
                .setTrans_country_code(oneLine[i++])//17
                .setTrans_originator_ip(oneLine[i++])//18
                .setCust_full_name(oneLine[i++])//19
                .setCust_first_name(oneLine[i++])//20
                .setCust_last_name(oneLine[i++])//21
                .setCust_address(oneLine[i++])//22
                .setCust_zip_code(oneLine[i++])//23
                .setCust_country_code(oneLine[i++])//24
                .setCust_identity_type(WordUtils.capitalize(oneLine[i++].toLowerCase()))//25
                .setCust_phone_number(oneLine[i++])//26
                .setCust_date_of_birth(oneLine[i++])//27
                .setCust_government_id(oneLine[i++])//28
                .setCust_government_id_type(oneLine[i++])//29
                .setCust_government_id_state(oneLine[i++])//30
                .setCust_driving_license(oneLine[i++])//31
                .setCust_driving_license_state(oneLine[i++])//32
                .setBank_fi_name(oneLine[i++]) //33
                .setBank_fi_routing(oneLine[i++])//34
                .setBank_fi_acc(oneLine[i++])//35
                .setBank_fi_acc_type(EnumHelper.from(oneLine[i++], BankAccountTypes.class).name())//36
                .setDevice_session_id(oneLine[i]) //37

                .setTrans_amount_dollars(formatAmt(oneLine[13]) / 100.00)
                .setTrans_amount_currency(oneLine[15])
                .setTrans_accumulator_amount_currency(oneLine[15])
                .build();
    }

    private DebitTransactionFinal getDebitTransactionFinal(String[] oneLine) {
        int i = 0;
        oneLine[18] = getTransSubType("V");
        return new DebitTransactionFinal.DebitTransactionFinalBuilder()
                .setEvent_type(oneLine[i++]) //0
                .setTimestamp(oneLine[i++])//1
                .setId(oneLine[i++])//2
                .setIdentity_key(oneLine[i++])//3
                .setBank_key(oneLine[i++])//4
                .setProduct_id(oneLine[i++])//5
                .setMerchant_id(oneLine[i++])//6
                .setMerchant_id_key(oneLine[i++])//7
                .setMerchant_user_id(oneLine[i++])//8
                .setMerchant_trans_id(oneLine[i++])//9
                .setTrans_id(oneLine[i++])//10
                .setTrans_type_id(EnumHelper.from(oneLine[i++], TransTypes.class).name())//11
                .setTrans_amount(formatAmt(oneLine[i++]))//12
                .setTrans_amount_converted(formatAmt(oneLine[i++]))//13
                .setTrans_currency(oneLine[i++])//14
                .setTrans_state(oneLine[i++]) //15
                .setTrans_country_code(oneLine[i++])//16
                .setTrans_datetime(oneLine[i++])//17
                .setTrans_subtype_id(oneLine[i++])//18
                .setTrans_status_id(EnumHelper.from(oneLine[i++], TransStatus.class).name())//19
                .setTrans_ext_reference_no(oneLine[i++])//20
                .setTrans_channel(EnumHelper.from(oneLine[i++], ProcessingChannelTypes.class).name())//21
                .setTrans_error_code(oneLine[i++])//22
                .setTrans_originator_ip(oneLine[i++])//23
                .setTrans_payee(oneLine[i++])//24
                .setCust_full_name(oneLine[i++])//25
                .setCust_first_name(oneLine[i++])//26
                .setCust_last_name(oneLine[i++])//27
                .setCust_address(oneLine[i++])//28
                .setCust_zip_code(oneLine[i++])//29
                .setCust_country_code(oneLine[i++])//30
                .setCust_identity_type(WordUtils.capitalize(oneLine[i++].toLowerCase()))//31
                .setCust_phone_number(oneLine[i++])//32
                .setCust_date_of_birth(oneLine[i++])//33
                .setCust_government_id(oneLine[i++])//34
                .setCust_government_id_type(oneLine[i++])//35
                .setCust_government_id_state(oneLine[i++])//36
                .setCust_driving_license(oneLine[i++])//37
                .setCust_driving_license_state(oneLine[i++])//38
                .setLogin_datetime(oneLine[i++])//39
                .setLogin_balance(oneLine[i++])//40
                .setLogin_balance_converted(oneLine[i++])//41
                .setLogin_balance_currency(oneLine[i++])//42
                .setBank_fi_routing(oneLine[i++])//43
                .setBank_fi_acc(oneLine[i++])//44
                .setBank_fi_acc_type(EnumHelper.from(oneLine[i], BankAccountTypes.class) == null ? null : EnumHelper.from(oneLine[i], BankAccountTypes.class).name())//45

                .setTrans_amount_dollars(formatAmt(oneLine[12]) / 100.00)
                .setTrans_amount_currency(oneLine[14])
                .setTrans_accumulator_amount_currency(oneLine[14])
                .build();
    }

    private DebitCheckTransaction getDebitCheckTransaction(String[] oneLine) {
        int i = 0;
        oneLine[20] = "Initiated";
        oneLine[19] = getTransSubType("A");
        return new DebitCheckTransaction.DebitCheckTransactionBuilder()
                .setEvent_type(oneLine[i++]) //0
                .setTimestamp(oneLine[i++])//1
                .setId(oneLine[i++])//2
                .setIdentity_key(oneLine[i++])//3
                .setBank_key(oneLine[i++])//4
                .setMerchant_customer_key(oneLine[i++])//5
                .setProduct_id(oneLine[i++])//6
                .setMerchant_id(oneLine[i++])//7
                .setMerchant_id_key(oneLine[i++])//8
                .setMerchant_user_id(oneLine[i++])//9
                .setMerchant_trans_id(oneLine[i++])//10
                .setTrans_id(oneLine[i++])//11
                .setTrans_type_id(EnumHelper.from(oneLine[i++], TransTypes.class).name())//12
                .setTrans_amount(formatAmt(oneLine[i++]))//13
                .setTrans_amount_converted(formatAmt(oneLine[i++]))//14
                .setTrans_currency(oneLine[i++])//15
                .setTrans_state(oneLine[i++]) //16
                .setTrans_country_code(oneLine[i++])//17
                .setTrans_datetime(oneLine[i++])//18
                .setTrans_subtype_id(oneLine[i++])//19
                .setTrans_status_id(oneLine[i++])//20
                .setTrans_ext_reference_no(oneLine[i++])//21
                .setTrans_channel(EnumHelper.from(oneLine[i++], ProcessingChannelTypes.class).name())//22
                .setTrans_error_code(oneLine[i++])//23
                .setTrans_originator_ip(oneLine[i++])//24
                .setTrans_payee(oneLine[i++])//25
                .setCust_full_name(oneLine[i++])//26
                .setCust_first_name(oneLine[i++])//27
                .setCust_last_name(oneLine[i++])//28
                .setCust_address(oneLine[i++])//29
                .setCust_zip_code(oneLine[i++])//30
                .setCust_country_code(oneLine[i++])//31
                .setCust_identity_type(WordUtils.capitalize(oneLine[i++].toLowerCase()))//32
                .setCust_phone_number(oneLine[i++])//33
                .setCust_date_of_birth(oneLine[i++])//34
                .setCust_government_id(oneLine[i++])//35
                .setCust_government_id_type(oneLine[i++])//36
                .setCust_government_id_state(oneLine[i++])//37
                .setCust_driving_license(oneLine[i++])//38
                .setCust_driving_license_state(oneLine[i++])//39
                .setLogin_datetime(oneLine[i++])//40
                .setLogin_balance(oneLine[i++])//41
                .setLogin_balance_converted(oneLine[i++])//42
                .setLogin_balance_currency(oneLine[i++])//43
                .setBank_fi_name(oneLine[i++]) //44
                .setBank_fi_routing(oneLine[i++])//45
                .setBank_fi_acc(oneLine[i++])//46
                .setBank_fi_acc_type(EnumHelper.from(oneLine[i], BankAccountTypes.class).name())//47

                .setTrans_amount_dollars(formatAmt(oneLine[13]) / 100.00)
                .setTrans_amount_currency(oneLine[15])
                .setTrans_accumulator_amount_currency(oneLine[15])
                .build();
    }

    private String getTransSubType(String verifyType) {

        return EnumHelper.from(verifyType, TransSubTypes.class).name();
    }
}
