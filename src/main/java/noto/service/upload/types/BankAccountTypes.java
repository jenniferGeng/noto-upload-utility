package noto.service.upload.types;

public enum BankAccountTypes implements IdEnum<String> {
    Checking("C"),
    Savings("S");

    private String id;

    BankAccountTypes(String id) {
        this.id = id;
    }
    @Override
    public String getId() {
        return id;
    }


}
