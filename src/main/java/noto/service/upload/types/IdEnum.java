package noto.service.upload.types;

public interface IdEnum<T> {

    T getId();
}
