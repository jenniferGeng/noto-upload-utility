package noto.service.upload.types;

public enum  ProcessingChannelTypes implements IdEnum<String> {
    Balance("I"),
    BatchFile("A");

    private String id;

    ProcessingChannelTypes(String id) {
        this.id = id;
    }
    @Override
    public String getId() {
        return id;
    }
}
