package noto.service.upload.types;

public enum TransSubTypes implements IdEnum<String> {

    IDVerified("I"),
    Unverified("U"),
    BankRealTime("R"),
    BankAcquired("A"),
    BankVerified("V");

    private String id;

    TransSubTypes(String id) {
        this.id = id;
    }
    @Override
    public String getId() {
        return id;
    }

}
