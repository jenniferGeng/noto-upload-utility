package noto.service.upload.types;

public enum TransStatus implements IdEnum<String> {
    Initiated("I"),
    Successful("S"),
    Failed("F"),
    Rejected("R"),
    Declined("X"),
    Canceled("C"),
    Abandoned("A");

    private String id;

    TransStatus(String id) {
        this.id = id;
    }
    @Override
    public String getId() {
        return id;
    }
}
