package noto.service.upload.types;

/**
 * Helper for managing enums.
 */
public class EnumHelper {
    /**
     * Maps any code to an enum. The enum class must implement {@link IdEnum}.
     *
     * @param id code to map
     * @param clz enum type
     * @param <T> enum type
     * @return the enumeration value or <code>null</code>
     */
    public static <E, T extends IdEnum<E>> T from(E id, Class<T> clz) {

        if (clz == null) {
            return null;
        }

        for (T p : clz.getEnumConstants()) {
            if (p.getId().equals(id)) {
                return p;
            }
        }

        return null;
    }

    public static <T extends String > T to(Class<T> clz) {
/*
        if (clz == null) {
            return null;
        }

        for (T p : clz.getEnumConstants()) {
            if (p.getId() == id) {
                return p;
            }
        }*/

        return null;
    }
}
