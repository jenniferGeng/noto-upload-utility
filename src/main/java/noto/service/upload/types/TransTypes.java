package noto.service.upload.types;

public enum TransTypes implements IdEnum {
    Debit("1"),
    Credit("3");

    private String id;

    TransTypes(String id) {
        this.id = id;
    }
    @Override
    public String getId() {
        return id;
    }

}
