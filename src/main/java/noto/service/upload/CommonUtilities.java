package noto.service.upload;

import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

public class CommonUtilities {
    public static final String baseUrl = "https://us-api.notormsgateway.com";
    public static final String IdentityOperationalDataList = "IdentityOperationalDataListV2";
    public static final String BankInformationOperationalDataList = "BankInformationOperationalDataListV2";
    public static final String IdentityBlacklist = "IdentityBlacklistV2";
    public static final String BankInformationBlackList = "BankInformationBlacklistV2";
    public static final String MerchantLimitRolling = "MerchantLimitRolling";
    public static final String MerchantVIPList = "MerchantVIPListV2";
    public static final String MerchantVIPArchivingList = "MerchantVIPArchivingListV2";
    public static final String LocalIPGeoMappingList = "LocalIPGeoMappingList";

    public static final String Debit_Check_Initial = "Debit_Check_Initial";
    public static final String Debit_Check_Transaction = "Debit_Check_Transaction";
    public static final String Debit_Transaction_Final = "Debit_Transaction_Final";

    public static final String Debit_Check_All_Rules = "Debit_Check_All_Rules";
    public static final String Debit_Check_Bank_Login = "Debit_Check_Bank_Login";

    public static final String Credit_Check_All_Rules = "Credit_Check_All_Rules";

    public static final String Return_Representment_Transaction_NSF1 = "Return_Representment_Transaction_NSF1";
    public static final String Return_Representment_Transaction_NSF2 = "Return_Representment_Transaction_NSF2";
    public static final String Return_Transaction_Final_Return = "Return_Transaction_Final_Return";

    private static final Logger logger = LoggerFactory.getLogger(CommonUtilities.class);

    public static void printFailedRecords(List<String> failedRecordList) {

        if (failedRecordList.isEmpty()) {
            logger.info("No failed item");
            return;
        }

        logger.info("\n\n-----The following are failed items------");
        failedRecordList.forEach(System.err::println);
    }


    public static String[] removeDoubleQuote4Array(String[] stringArr) {
        return Arrays.stream(stringArr).map(a -> removeDoubleQuotes(a)).toArray(String[] :: new);
    }

    public static String removeDoubleQuotes(String string) {
        return string.replaceAll("\"", "");
    }

    public static String encodeSpace(String string) {
        return string.replace("+", "%20");
    }

    public static String encodeAsterisk(String string) {
        return string.replace("*", "%2A");
    }

    public static String encodeSpecialCharacters(String string) {
        return encodeAsterisk(encodeSpace(string));
    }

    public static String[] splitBy(String string, String separator) {
        int commaPos = string.indexOf(",", string.lastIndexOf("|")); //get comma position after the last |

        String keyList = string.substring(0, commaPos);
        String[] array = string.substring(keyList.length() + 1).split(separator);

        String[] longer = new String[array.length + 1];
        System.arraycopy(array, 0, longer, 1, array.length);

        longer[0] = keyList;

        return longer;
    }

    public static Long formatAmt(String amount) {
        Long finalAmt = null;

        if (amount != null && !StringUtils.isEmpty(amount)) {
            int decimalPos = amount.lastIndexOf(".");
            if (decimalPos != -1) {
                String realAmount = amount.substring(0, decimalPos);
                finalAmt = Long.valueOf(realAmount);

            } else {
                finalAmt = Long.valueOf(amount);
            }
        }

        return finalAmt;
    }

    private CommonUtilities(){}
}
