package noto.service.upload;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.springframework.stereotype.Component;

@Component
public class LoadProperties {

    static Properties prop = null;

    public static String fileNmae;
    public static String accessCode;
    public static String divisionFullName;
    public static int expectedColumnNumber;
    public static int idColumnIndex;
    public static String listId;
    public static String eventType;

    static {
        try (InputStream input = UploadTransactions.class.getClassLoader().getResourceAsStream("config.properties")) {

            prop = new Properties();

            // load a properties file
            prop.load(input);

            fileNmae = prop.getProperty("file.name");
            accessCode = prop.getProperty("access.token");
            divisionFullName = prop.getProperty("division.full.name");

            listId = prop.getProperty("list.id");
            eventType = prop.getProperty("event.type");

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private LoadProperties(){}
}
