package noto.service.upload;


import static noto.service.upload.CommonUtilities.BankInformationBlackList;
import static noto.service.upload.CommonUtilities.BankInformationOperationalDataList;
import static noto.service.upload.CommonUtilities.IdentityOperationalDataList;
import static noto.service.upload.CommonUtilities.IdentityBlacklist;
import static noto.service.upload.CommonUtilities.LocalIPGeoMappingList;
import static noto.service.upload.CommonUtilities.MerchantLimitRolling;
import static noto.service.upload.CommonUtilities.MerchantVIPArchivingList;
import static noto.service.upload.CommonUtilities.MerchantVIPList;
import static noto.service.upload.CommonUtilities.baseUrl;
import static noto.service.upload.CommonUtilities.encodeSpecialCharacters;
import static noto.service.upload.CommonUtilities.printFailedRecords;
import static noto.service.upload.CommonUtilities.removeDoubleQuotes;
import static java.net.URLEncoder.encode;
import static noto.service.upload.LoadProperties.accessCode;
import static noto.service.upload.LoadProperties.divisionFullName;
import static noto.service.upload.LoadProperties.expectedColumnNumber;
import static noto.service.upload.LoadProperties.fileNmae;
import static noto.service.upload.LoadProperties.idColumnIndex;
import static noto.service.upload.LoadProperties.listId;

import java.util.stream.Stream;
import noto.service.upload.model.list.LocalIPGeoMappingList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.io.File;
import noto.service.upload.model.list.BankInformationBlackList;
import noto.service.upload.model.list.IdentityBlacklist;
import noto.service.upload.model.list.IdentityOperationalData;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import noto.service.upload.model.list.MerchantLimitRolling;
import noto.service.upload.model.list.MerchantVIPList;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class UploadDataList implements INOTOUpload {

    public static final String baseUri = baseUrl + "/1.0/list/division-type/values/";
    public static final String separator = ",";

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    public UploadWebClient client = new UploadWebClient();

    @Override
    public void upload() throws IOException {
        logger.info(listId);
        setValues();

        AtomicInteger lineNum = new AtomicInteger();
        AtomicInteger numberOfAdded = new AtomicInteger();
        AtomicInteger numberOfFailed = new AtomicInteger();
        AtomicInteger total = new AtomicInteger();
        List<String> failedRecordList = new ArrayList<>();

        String filePath = new File("").getAbsolutePath() + "/src/main/resources/listFiles/" + fileNmae;
        try (Stream<String> lines = Files.lines(Paths.get(filePath))) {
            lines.forEach(line -> {

                //String[] oneLine = splitBy(removeDoubleQuotes(line), separator);
                String[] oneLine = removeDoubleQuotes(line).split(separator, -1); //second parameter -1 means we want to keep the empty string as part of the array

                lineNum.getAndIncrement();

                if (lineNum.get() > 1) {

                    if (!StringUtils.isEmpty(oneLine)) {
                        total.getAndIncrement();

                        String listKey = oneLine[idColumnIndex];

                        if (oneLine.length != expectedColumnNumber) {
                            System.err.println(lineNum + ": " + listKey + " does not have enough data!");
                            numberOfFailed.getAndIncrement();
                            failedRecordList.add(line);

                        } else {
                            try {
                                String encodedKey = encodeSpecialCharacters(encode(listKey, StandardCharsets.UTF_8.toString()));

                                String notoResourceUrl = baseUri + listId + "/" + encodedKey + "?division=" + divisionFullName;

                                ResponseEntity<String> resp = getResponseEntity(listId, oneLine, notoResourceUrl);

                                if (resp != null && resp.getStatusCode() == HttpStatus.OK) {
                                    numberOfAdded.getAndIncrement();
                                    logger.info(lineNum + ": " + listKey + " ---- " + " Upload Successful");

                                } else {
                                    numberOfFailed.getAndIncrement();
                                    System.err.println(
                                            lineNum + ": " + oneLine[0] + "," + oneLine[1] + "," + oneLine[2] + "," + oneLine[3] + " Upload Failed. " + "Status code: " + resp.getStatusCode());
                                    failedRecordList.add(line);
                                }
                            } catch (Exception e) {
                                numberOfFailed.getAndIncrement();
                                System.err.println(
                                        lineNum + ": " + oneLine[0] + "," + oneLine[1] + "," + oneLine[2] + "," + oneLine[3] + " Upload Failed. " + e.getMessage());
                                failedRecordList.add(line);
                                e.printStackTrace();
                            }
                        }

                        try {
                            Thread.sleep(10);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                    } else {
                        logger.info(lineNum + ": is empty line");
                    }
                }
            });
        }

        logger.info("Done! There are total " + total + " items. " + numberOfAdded + " item(s) were added successfully...." + numberOfFailed + " item(s) failed.");

        printFailedRecords(failedRecordList);
    }

    private static void setValues() {
        switch (listId) {
            case IdentityOperationalDataList:

            case BankInformationOperationalDataList:
                expectedColumnNumber = 4;
                idColumnIndex = 0;
                break;
            case IdentityBlacklist:
                expectedColumnNumber = 19;
                idColumnIndex = 1;
                break;

            case BankInformationBlackList:
                expectedColumnNumber = 15;
                idColumnIndex = 1;
                break;

            case MerchantLimitRolling:
                expectedColumnNumber = 15;
                idColumnIndex = 0;
                break;

            case MerchantVIPArchivingList:

            case MerchantVIPList:
                expectedColumnNumber = 11;
                idColumnIndex = 1;
                break;

            case LocalIPGeoMappingList:
                expectedColumnNumber = 7;
                idColumnIndex = 0;
                break;

            default:
        }
    }


    private ResponseEntity<String> getResponseEntity(String listId, String[] oneLine, String notoResourceUrl) {
        ResponseEntity<String> response = null;

        switch (listId) {
            case IdentityOperationalDataList:

            case BankInformationOperationalDataList:
                IdentityOperationalData identityOperationalData = new IdentityOperationalData(oneLine[1], oneLine[2], oneLine[3]);
                response = client.getResponseEntity(notoResourceUrl, accessCode, identityOperationalData);

                break;
            case IdentityBlacklist:
                IdentityBlacklist identityBlacklist = new IdentityBlacklist.IdentityBlacklistBuilder()
                        .set$listItemId(oneLine[1])
                        .setEvent_type(oneLine[0])
                        .setMerchant_customer_key(oneLine[2])
                        .setCust_last_name(oneLine[3])
                        .setCust_zip_code(oneLine[4])
                        .setCust_date_of_birth(oneLine[5])
                        .setCust_country_code(oneLine[6])
                        .setCust_government_id(oneLine[7])
                        .setCust_government_id_type(oneLine[8])
                        .setCust_government_id_state(oneLine[9])
                        .setCust_phone_number(oneLine[10])
                        .setProduct_id(oneLine[11])
                        .setMerchant_id(oneLine[12])
                        .setTransaction_id(oneLine[13])
                        .setReason_type(oneLine[14])
                        .setReason_code(oneLine[15])
                        .setLast_seen_datetime(oneLine[16])
                        .setCreated_datetime(oneLine[17])
                        .set$Timestamp(oneLine[18])
                        .build();

                response = client.getResponseEntity(notoResourceUrl, accessCode, identityBlacklist);
                break;

            case BankInformationBlackList:
                BankInformationBlackList bankInformationBlackList = new BankInformationBlackList.BankInformationBlackListBuilder()
                        .setEvent_type(oneLine[0])
                        .set$listItemId(oneLine[1])
                        .setMerchant_customer_key(oneLine[2])
                        .setCust_country_code(oneLine[3])
                        .setBank_fi_routing(oneLine[4])
                        .setBank_fi_account(oneLine[5])
                        .setBank_fi_account_type(oneLine[6])
                        .setProduct_id(oneLine[7])
                        .setMerchant_id(oneLine[8])
                        .setTransaction_id(oneLine[9])
                        .setReason_type(oneLine[10])
                        .setReason_code(oneLine[11])
                        .setLast_seen_datetime(oneLine[12])
                        .setCreated_datetime(oneLine[13])
                        .set$timestamp(oneLine[14])
                        .build();
                response = client.getResponseEntity(notoResourceUrl, accessCode, bankInformationBlackList);
                break;

            case MerchantLimitRolling:
                MerchantLimitRolling merchantLimitRolling = new MerchantLimitRolling.MerchantLimitRollingBuilder()
                        .setEvent_type(oneLine[0])
                        .set$listItemId(oneLine[1])
                        .setProduct_id(oneLine[2])
                        .setMerchant_id(oneLine[3])
                        .setRisk_rating(oneLine[4])
                        .setRolling_period_in_days(oneLine[5])
                        .setThreshold_amount(oneLine[6])
                        .setThreshold_count(oneLine[7])
                        .setLimit_level_amount(oneLine[8])
                        .setLimit_level_count(oneLine[9])
                        .setIs_vip_risk_rating(oneLine[10])
                        .setIs_enabled(oneLine[11])
                        .setAdmin_id(oneLine[12])
                        .setCreated_datetime(oneLine[13])
                        .set$timestamp(oneLine[14])
                        .build();

                response = client.getResponseEntity(notoResourceUrl, accessCode, merchantLimitRolling);
                break;

            case MerchantVIPArchivingList:

            case MerchantVIPList:
                MerchantVIPList merchantVIPList = new MerchantVIPList.MerchantVIPListBuilder()
                        .setEvent_type(oneLine[0])
                        .set$listItemId(oneLine[1])
                        .setProduct_id(oneLine[2])
                        .setMerchant_id(oneLine[3])
                        .setMerchant_user_id(oneLine[4])
                        .setRisk_rating(oneLine[5])
                        .setStatus(oneLine[6])
                        .setAdmin_id(oneLine[7])
                        .setLast_seen_datetime(oneLine[8])
                        .setCreated_datetime(oneLine[9])
                        .set$timestamp(oneLine[10])
                        .build();
                response = client.getResponseEntity(notoResourceUrl, accessCode, merchantVIPList);
                break;

            case LocalIPGeoMappingList:
                LocalIPGeoMappingList localIPGeoMappingList = new LocalIPGeoMappingList();
                localIPGeoMappingList.set$listItemId(oneLine[0]);
                localIPGeoMappingList.setCity(oneLine[1]);
                localIPGeoMappingList.setCountry(oneLine[2]);
                localIPGeoMappingList.setCreated_datetime(oneLine[3]);
                localIPGeoMappingList.setNotes(oneLine[4]);
                localIPGeoMappingList.setState(oneLine[5]);
                localIPGeoMappingList.set$timestamp(oneLine[6]);
                response = client.getResponseEntity(notoResourceUrl, accessCode, localIPGeoMappingList);
                break;

            default:

        }
        return response;
    }
}
