


-- Review rules that need to run for history - think its just the runprofiles



-- Acquire
-- 1. Debit Check Initial Query
WITH 
key_data(trans_id, transaction_key, identity_key, bank_key, merchant_customer_key) AS 
(
  SELECT x.trans_id,
         'ECHECKSELECT|' || hex(x.trans_id) AS transaction_key,
  		 'US|' || upper(x.last_name) || '|'  || upper(x.postal_code) || '|' || x.dob identity_key,
         CASE WHEN nullif(x.fi_routing, '') IS NOT NULL AND x.fi_acc NOT LIKE '%**%' THEN 'US|' || x.fi_acc || '|' || x.fi_routing || '|' || CASE WHEN x.fi_acc_type_id = 'C' THEN 'CHECKING' WHEN x.fi_acc_type_id = 'S' THEN 'SAVINGS' ELSE '' END ELSE NULL END AS bank_key,
         'ECHECKSELECT|' || upper(x.merchant_acc_no) || '|' || upper(x.merchant_customer_id) AS merchant_customer_key
  FROM db2admin.ach_detail_trans x
) 
SELECT 
	   'debit_check_initial' AS event_type,
--	   adt.TRANS_DATETIME ,
       CAST(date(adt.trans_datetime) AS varchar(32)) || 'T' || replace(CAST(time(adt.trans_datetime) AS varchar(32)), '.', ':') || '.' || CAST(microsecond(adt.trans_datetime) AS varchar(8)) || 'Z' AS trans_datetime, -- UTC

       CAST(date(adt.trans_datetime - current timezone) AS varchar(32)) || 'T' || replace(CAST(time(adt.trans_datetime - current timezone) AS varchar(32)), '.', ':') || '.' || CAST(microsecond(adt.trans_datetime) AS varchar(8)) || 'Z' AS $timestamp, -- UTC
       kd.transaction_key AS id,
       kd.identity_key,
       kd.merchant_customer_key,
	   'ECHECKSELECT' AS product_id,
       adt.merchant_acc_no AS merchant_id,
       upper(adt.merchant_acc_no) AS merchant_id_key,
       adt.merchant_customer_id AS merchant_user_id,
       adt.merchant_trans_id,
       NULL AS device_session_id,
       adt.originator_ip AS trans_originator_ip
FROM db2admin.ach_detail_trans adt
INNER JOIN key_data kd ON kd.trans_id = adt.trans_id
LEFT JOIN db2admin.big_log bl ON bl.trans_id = adt.trans_id
WHERE bl.echeck_type_id = 'A' AND adt.trans_type_id = '1' AND adt.merchant_customer_id IS NOT NULL AND adt.dob IS NOT NULL 
	AND (
		(adt.trans_datetime BETWEEN (TIMESTAMP('2021-04-19 00:23:00.000') + 3 HOURS) 
							AND (TIMESTAMP('2021-04-19 02:57:47.000') + 3 HOURS))
		OR (adt.trans_datetime BETWEEN (TIMESTAMP('2021-04-19 18:20:00.000') + 3 HOURS) 
							AND (TIMESTAMP('2021-04-21 09:36:00.000') + 3 HOURS))
		)	
ORDER BY adt.trans_datetime
;


-- Acquire
-- 2. Debit Check Transaction Query
WITH 
key_data(trans_id, transaction_key, identity_key, bank_key, merchant_customer_key) AS 
(
  SELECT x.trans_id,
         'ECHECKSELECT|' || hex(x.trans_id) AS transaction_key,
  		 'US|' || upper(x.last_name) || '|'  || upper(x.postal_code) || '|' || x.dob identity_key,
         CASE WHEN nullif(x.fi_routing, '') IS NOT NULL AND x.fi_acc NOT LIKE '%**%' THEN 'US|' || x.fi_acc || '|' || x.fi_routing || '|' || CASE WHEN x.fi_acc_type_id = 'C' THEN 'CHECKING' WHEN x.fi_acc_type_id = 'S' THEN 'SAVINGS' ELSE '' END ELSE NULL END AS bank_key,
         'ECHECKSELECT|' || upper(x.merchant_acc_no) || '|' || upper(x.merchant_customer_id) AS merchant_customer_key
  FROM db2admin.ach_detail_trans x
)
SELECT 
	   'debit_check_transaction' AS event_type,
       CAST(date(adt.trans_datetime - current timezone) AS varchar(32)) || 'T' || replace(CAST(time(adt.trans_datetime - current timezone) AS varchar(32)), '.', ':') || '.' || CAST(microsecond(adt.trans_datetime) AS varchar(8)) || 'Z' AS $timestamp, -- UTC
       kd.transaction_key AS id,
       kd.identity_key,
       kd.bank_key,
       kd.merchant_customer_key,
	   'ECHECKSELECT' AS product_id,
       adt.merchant_acc_no AS merchant_id,
       upper(adt.merchant_acc_no) AS merchant_id_key,
       adt.merchant_customer_id AS merchant_user_id,
       adt.merchant_trans_id,
       hex(adt.trans_id) trans_id,
       adt.trans_type_id,
       adt.trans_amt * 100 AS trans_amount,
       adt.trans_amt * 100 AS trans_amount_converted,
       adt.currency_code AS trans_currency,
       CASE WHEN adt.tax_state IS NOT NULL THEN adt.tax_state ELSE 'NJ' END AS trans_state,
       adt.country AS trans_country_code,
       CAST(date(adt.trans_datetime - current timezone - 5 MINUTES) AS varchar(32)) || 'T' || replace(CAST(time(adt.trans_datetime - current timezone - 5 MINUTES) AS varchar(32)), '.', ':') || '.' || CAST(microsecond(adt.trans_datetime) AS varchar(8)) || 'Z' AS trans_datetime, -- UTC
       adt.sub_trans_type_id AS trans_subtype_id,
       adt.trans_status_id,
       adt.ext_reference_no AS trans_ext_reference_no,
       adt.channel AS trans_channel,
       COALESCE(mn.error_code, adt.trans_detail) AS trans_error_code,
       adt.originator_ip AS trans_originator_ip,
       adt.payee AS trans_payee,
       initcap(adt.first_name) || ' ' || initcap(adt.last_name) AS cust_full_name,
       initcap(adt.first_name) AS cust_first_name,
       initcap(adt.last_name) AS cust_last_name,
       CASE WHEN adt.line1 IS NOT NULL THEN adt.line1 || ', ' ELSE '' END || CASE WHEN adt.line2 IS NOT NULL THEN adt.line2 || ', ' ELSE '' end || CASE WHEN adt.city IS NOT NULL THEN adt.city || ', ' ELSE '' END || adt.postal_code AS cust_address,
       adt.postal_code AS cust_zip_code,
       adt.country AS cust_country_code,
       'INDIVIDUAL' AS cust_identity_type,
       NULLIF(NULLIF(adt.hph, 'nullnull'), '') AS cust_phone_number,
       adt.dob AS cust_date_of_birth,
       NULLIF(adt.ssn, '') AS cust_government_id,
       CASE WHEN NULLIF(adt.ssn, '') IS NULL THEN NULL ELSE 'SSN' END AS cust_government_id_type,
       NULL AS cust_government_id_state,
       adt.drv_lic_no AS cust_driving_license,
       adt.drv_lic_state AS cust_driving_license_state,
       CAST(date(bl.log_date - current timezone - 5 MINUTES) AS varchar(32)) || 'T' || replace(CAST(time(bl.log_date - current timezone - 5 MINUTES) AS varchar(32)), '.', ':') || '.' || CAST(microsecond(bl.log_date) AS varchar(8)) || 'Z' AS login_datetime, -- UTC
       bl.customer_account_balance AS login_balance,
       bl.customer_account_balance AS login_balance_converted,
       bl.customer_account_balance_currency AS login_balance_currency,
       bl.bank_name AS bank_fi_name,
       adt.fi_routing AS bank_fi_routing,
       adt.fi_acc AS bank_fi_acc,
       adt.fi_acc_type_id AS bank_fi_acc_type
FROM db2admin.ach_detail_trans adt
INNER JOIN key_data kd ON kd.trans_id = adt.trans_id
LEFT JOIN db2admin.big_log bl ON bl.trans_id = adt.trans_id
LEFT JOIN db2admin.merchant_ntfy mn ON mn.trans_id = adt.trans_id 
WHERE bl.echeck_type_id = 'A' AND adt.trans_type_id IN ('1', '3') AND adt.merchant_customer_id IS NOT NULL AND adt.dob IS NOT NULL AND adt.trans_status_id = 'S'
	AND (
		(adt.trans_datetime BETWEEN (TIMESTAMP('2021-04-19 00:23:00.000') + 3 HOURS) 
							AND (TIMESTAMP('2021-04-19 02:57:47.000') + 3 HOURS))
		OR (adt.trans_datetime BETWEEN (TIMESTAMP('2021-04-19 18:20:00.000') + 3 HOURS) 
							AND (TIMESTAMP('2021-04-21 09:36:00.000') + 3 HOURS))
		)	
ORDER BY adt.trans_datetime
;

-- Acquire
-- 3. Debit Transaction Final Query
WITH 
key_data(trans_id, transaction_key, identity_key, bank_key, merchant_customer_key) AS 
(
  SELECT x.trans_id,
         'ECHECKSELECT|' || hex(x.trans_id) AS transaction_key,
  		 'US|' || upper(x.last_name) || '|'  || upper(x.postal_code) || '|' || x.dob identity_key,
         CASE WHEN nullif(x.fi_routing, '') IS NOT NULL AND x.fi_acc NOT LIKE '%**%' THEN 'US|' || x.fi_acc || '|' || x.fi_routing || '|' || CASE WHEN x.fi_acc_type_id = 'C' THEN 'CHECKING' WHEN x.fi_acc_type_id = 'S' THEN 'SAVINGS' ELSE '' END ELSE NULL END AS bank_key,
         'ECHECKSELECT|' || upper(x.merchant_acc_no) || '|' || upper(x.merchant_customer_id) AS merchant_customer_key
  FROM db2admin.ach_detail_trans x
)
SELECT 
	   'debit_transaction_final' AS event_type,
       CAST(date(adt.trans_datetime - current timezone) AS varchar(32)) || 'T' || replace(CAST(time(adt.trans_datetime - current timezone) AS varchar(32)), '.', ':') || '.' || CAST(microsecond(adt.trans_datetime) AS varchar(8)) || 'Z' AS $timestamp, -- UTC
       kd.transaction_key AS id,
       kd.identity_key,
       kd.bank_key,
	   'ECHECKSELECT' AS product_id,
       adt.merchant_acc_no AS merchant_id,
       upper(adt.merchant_acc_no) AS merchant_id_key,
       adt.merchant_customer_id AS merchant_user_id,
       adt.merchant_trans_id,
       hex(adt.trans_id) trans_id,
       adt.trans_type_id,
       adt.trans_amt * 100 AS trans_amount,
       adt.trans_amt * 100 AS trans_amount_converted,
       adt.currency_code AS trans_currency,
       CASE WHEN adt.tax_state IS NOT NULL THEN adt.tax_state ELSE 'NJ' END AS trans_state,
       adt.country AS trans_country_code,
       CAST(date(adt.trans_datetime) AS varchar(32)) || 'T' || replace(CAST(time(adt.trans_datetime) AS varchar(32)), '.', ':') || '.' || CAST(microsecond(adt.trans_datetime) AS varchar(8)) || 'Z' AS trans_datetime, -- EST
       adt.sub_trans_type_id AS trans_subtype_id,
       adt.trans_status_id,
       adt.ext_reference_no AS trans_ext_reference_no,
       adt.channel AS trans_channel,
       COALESCE(mn.error_code, adt.trans_detail) AS trans_error_code,
       adt.originator_ip AS trans_originator_ip,
       adt.payee AS trans_payee,
       initcap(adt.first_name) || ' ' || initcap(adt.last_name) AS cust_full_name,
       initcap(adt.first_name) AS cust_first_name,
       initcap(adt.last_name) AS cust_last_name,
       CASE WHEN adt.line1 IS NOT NULL THEN adt.line1 || ', ' ELSE '' END || CASE WHEN adt.line2 IS NOT NULL THEN adt.line2 || ', ' ELSE '' end || CASE WHEN adt.city IS NOT NULL THEN adt.city || ', ' ELSE '' END || adt.postal_code AS cust_address,
       adt.postal_code AS cust_zip_code,
       adt.country AS cust_country_code,
       'INDIVIDUAL' AS cust_identity_type,
       NULLIF(NULLIF(adt.hph, 'nullnull'), '') AS cust_phone_number,
       adt.dob AS cust_date_of_birth,
       NULLIF(adt.ssn, '') AS cust_government_id,
       CASE WHEN NULLIF(adt.ssn, '') IS NULL THEN NULL ELSE 'SSN' END AS cust_government_id_type,
       NULL AS cust_government_id_state,
       adt.drv_lic_no AS cust_driving_license,
       adt.drv_lic_state AS cust_driving_license_state,
       CAST(date(bl.log_date - current timezone - 5 MINUTES) AS varchar(32)) || 'T' || replace(CAST(time(bl.log_date - current timezone - 5 MINUTES) AS varchar(32)), '.', ':') || '.' || CAST(microsecond(bl.log_date) AS varchar(8)) || 'Z' AS login_datetime, -- UTC
       bl.customer_account_balance AS login_balance,
       bl.customer_account_balance AS login_balance_converted,
       bl.customer_account_balance_currency AS login_balance_currency,
       adt.fi_routing AS bank_fi_routing,
       adt.fi_acc AS bank_fi_acc,
       adt.fi_acc_type_id AS bank_fi_acc_type
FROM db2admin.ach_detail_trans adt
INNER JOIN key_data kd ON kd.trans_id = adt.trans_id
LEFT JOIN db2admin.big_log bl ON bl.trans_id = adt.trans_id
LEFT JOIN db2admin.merchant_ntfy mn ON mn.trans_id = adt.trans_id 
WHERE bl.echeck_type_id = 'A' AND adt.trans_type_id = '1' AND adt.merchant_customer_id IS NOT NULL AND adt.dob IS NOT NULL 
	AND (
		(adt.trans_datetime BETWEEN (TIMESTAMP('2021-04-19 00:23:00.000') + 3 HOURS) 
							AND (TIMESTAMP('2021-04-19 02:57:47.000') + 3 HOURS))
		OR (adt.trans_datetime BETWEEN (TIMESTAMP('2021-04-19 18:20:00.000') + 3 HOURS) 
							AND (TIMESTAMP('2021-04-21 09:36:00.000') + 3 HOURS))
		)	
ORDER BY adt.trans_datetime
;

-- Validate
-- 4. Debit Check All Rules Query
WITH 
key_data(trans_id, transaction_key, identity_key, bank_key, merchant_customer_key) AS 
(
  SELECT x.trans_id,
         'ECHECKSELECT|' || hex(x.trans_id) AS transaction_key,
  		 'US|' || upper(x.last_name) || '|'  || upper(x.postal_code) || '|' || x.dob identity_key,
         CASE WHEN nullif(x.fi_routing, '') IS NOT NULL AND x.fi_acc NOT LIKE '%**%' THEN 'US|' || x.fi_acc || '|' || x.fi_routing || '|' || CASE WHEN x.fi_acc_type_id = 'C' THEN 'CHECKING' WHEN x.fi_acc_type_id = 'S' THEN 'SAVINGS' ELSE '' END ELSE NULL END AS bank_key,
         'ECHECKSELECT|' || upper(x.merchant_acc_no) || '|' || upper(x.merchant_customer_id) AS merchant_customer_key
  FROM db2admin.ach_detail_trans x
)
SELECT 
	   'debit_check_all_rules' AS event_type,
       CAST(date(adt.trans_datetime - current timezone) AS varchar(32)) || 'T' || replace(CAST(time(adt.trans_datetime - current timezone) AS varchar(32)), '.', ':') || '.' || CAST(microsecond(adt.trans_datetime) AS varchar(8)) || 'Z' AS $timestamp, -- UTC
       kd.transaction_key AS id,
       kd.identity_key,
       kd.bank_key,
       kd.merchant_customer_key,
	   'ECHECKSELECT' AS product_id,
       adt.merchant_acc_no AS merchant_id,
       upper(adt.merchant_acc_no) AS merchant_id_key,
       adt.merchant_customer_id AS merchant_user_id,
       adt.merchant_trans_id,
       hex(adt.trans_id) trans_id,
       adt.trans_type_id,
       adt.trans_amt * 100 AS trans_amount,
       adt.trans_amt * 100 AS trans_amount_converted,
       adt.currency_code AS trans_currency,
       CASE WHEN adt.tax_state IS NOT NULL THEN adt.tax_state ELSE 'NJ' END AS trans_state,
       adt.country AS trans_country_code,
       adt.originator_ip AS trans_originator_ip,
       initcap(adt.first_name) || ' ' || initcap(adt.last_name) AS cust_full_name,
       initcap(adt.first_name) AS cust_first_name,
       initcap(adt.last_name) AS cust_last_name,
       CASE WHEN adt.line1 IS NOT NULL THEN adt.line1 || ', ' ELSE '' END || CASE WHEN adt.line2 IS NOT NULL THEN adt.line2 || ', ' ELSE '' end || CASE WHEN adt.city IS NOT NULL THEN adt.city || ', ' ELSE '' END || adt.postal_code AS cust_address,
       adt.postal_code AS cust_zip_code,
       adt.country AS cust_country_code,
       'INDIVIDUAL' AS cust_identity_type,
       NULLIF(NULLIF(adt.hph, 'nullnull'), '') AS cust_phone_number,
       CAST(adt.dob AS varchar(32)) || 'T00:00:00.000000Z' AS cust_date_of_birth, -- UTC
       NULLIF(adt.ssn, '') AS cust_government_id,
       CASE WHEN NULLIF(adt.ssn, '') IS NULL THEN NULL ELSE 'SSN' END AS cust_government_id_type,
       NULL AS cust_government_id_state,
       adt.drv_lic_no AS cust_driving_license,
       adt.drv_lic_state AS cust_driving_license_state,
       bl.bank_name AS bank_fi_name,
       adt.fi_routing AS bank_fi_routing,
       adt.fi_acc AS bank_fi_acc,
       adt.fi_acc_type_id AS bank_fi_acc_type,
       NULL AS device_session_id,
       adt.originator_ip AS trans_originator_ip
FROM db2admin.ach_detail_trans adt
INNER JOIN key_data kd ON kd.trans_id = adt.trans_id
LEFT JOIN db2admin.big_log bl ON bl.trans_id = adt.trans_id
WHERE bl.echeck_type_id = 'V' AND adt.trans_type_id = '1' AND adt.merchant_customer_id IS NOT NULL AND adt.dob IS NOT NULL 
	AND (
		(adt.trans_datetime BETWEEN (TIMESTAMP('2021-04-19 00:23:00.000') + 3 HOURS) 
							AND (TIMESTAMP('2021-04-19 02:57:47.000') + 3 HOURS))
		OR (adt.trans_datetime BETWEEN (TIMESTAMP('2021-04-19 18:20:00.000') + 3 HOURS) 
							AND (TIMESTAMP('2021-04-21 09:36:00.000') + 3 HOURS))
		)	
ORDER BY adt.trans_datetime
;


-- Validate
-- 5. Debit Check Bank Login Query
WITH 
key_data(trans_id, transaction_key, identity_key, bank_key, merchant_customer_key) AS 
(
  SELECT x.trans_id,
         'ECHECKSELECT|' || hex(x.trans_id) AS transaction_key,
  		 'US|' || upper(x.last_name) || '|'  || upper(x.postal_code) || '|' || x.dob identity_key,
         CASE WHEN nullif(x.fi_routing, '') IS NOT NULL AND x.fi_acc NOT LIKE '%**%' THEN 'US|' || x.fi_acc || '|' || x.fi_routing || '|' || CASE WHEN x.fi_acc_type_id = 'C' THEN 'CHECKING' WHEN x.fi_acc_type_id = 'S' THEN 'SAVINGS' ELSE '' END ELSE NULL END AS bank_key,
         'ECHECKSELECT|' || upper(x.merchant_acc_no) || '|' || upper(x.merchant_customer_id) AS merchant_customer_key
  FROM db2admin.ach_detail_trans x
)
SELECT 
	   'debit_check_bank_login' AS event_type,
       CAST(date(adt.trans_datetime - current timezone) AS varchar(32)) || 'T' || replace(CAST(time(adt.trans_datetime - current timezone) AS varchar(32)), '.', ':') || '.' || CAST(microsecond(adt.trans_datetime) AS varchar(8)) || 'Z' AS $timestamp, -- UTC
       kd.transaction_key AS id,
       kd.identity_key,
       kd.bank_key,
       adt.merchant_acc_no AS merchant_id,
       upper(adt.merchant_acc_no) AS merchant_id_key,
       adt.merchant_customer_id AS merchant_user_id,
       bl.customer_account_balance - (adt.trans_amt * 100) AS trans_accumulator_amount,
       bl.customer_account_balance - (adt.trans_amt * 100) AS trans_accumulator_amount_converted,
       adt.currency_code AS trans_accumulator_amount_currency,
       initcap(adt.last_name) AS cust_last_name,
       CAST(date(bl.log_date - current timezone - 5 MINUTES) AS varchar(32)) || 'T' || replace(CAST(time(bl.log_date - current timezone - 5 MINUTES) AS varchar(32)), '.', ':') || '.' || CAST(microsecond(bl.log_date) AS varchar(8)) || 'Z' AS login_datetime, -- UTC
       bl.customer_account_balance AS login_balance,
       bl.customer_account_balance AS login_balance_converted,
       bl.customer_account_balance_currency AS login_balance_currency
FROM db2admin.ach_detail_trans adt
INNER JOIN key_data kd ON kd.trans_id = adt.trans_id
LEFT JOIN db2admin.big_log bl ON bl.trans_id = adt.trans_id
WHERE bl.echeck_type_id = 'V' AND adt.trans_type_id = '1' AND adt.merchant_customer_id IS NOT NULL AND adt.dob IS NOT NULL 
	AND (
		(adt.trans_datetime BETWEEN (TIMESTAMP('2021-04-19 00:23:00.000') + 3 HOURS) 
							AND (TIMESTAMP('2021-04-19 02:57:47.000') + 3 HOURS))
		OR (adt.trans_datetime BETWEEN (TIMESTAMP('2021-04-19 18:20:00.000') + 3 HOURS) 
							AND (TIMESTAMP('2021-04-21 09:36:00.000') + 3 HOURS))
		)	
ORDER BY adt.trans_datetime
;


-- Validate
-- 6. Debit Transaction Final Query
WITH 
key_data(trans_id, transaction_key, identity_key, bank_key, merchant_customer_key) AS 
(
  SELECT x.trans_id,
         'ECHECKSELECT|' || hex(x.trans_id) AS transaction_key,
  		 'US|' || upper(x.last_name) || '|'  || upper(x.postal_code) || '|' || x.dob identity_key,
         CASE WHEN nullif(x.fi_routing, '') IS NOT NULL AND x.fi_acc NOT LIKE '%**%' THEN 'US|' || x.fi_acc || '|' || x.fi_routing || '|' || CASE WHEN x.fi_acc_type_id = 'C' THEN 'CHECKING' WHEN x.fi_acc_type_id = 'S' THEN 'SAVINGS' ELSE '' END ELSE NULL END AS bank_key,
         'ECHECKSELECT|' || upper(x.merchant_acc_no) || '|' || upper(x.merchant_customer_id) AS merchant_customer_key
  FROM db2admin.ach_detail_trans x
)
SELECT 
	   'debit_transaction_final' AS event_type,
       CAST(date(adt.trans_datetime - current timezone) AS varchar(32)) || 'T' || replace(CAST(time(adt.trans_datetime - current timezone) AS varchar(32)), '.', ':') || '.' || CAST(microsecond(adt.trans_datetime) AS varchar(8)) || 'Z' AS $timestamp, -- UTC
       kd.transaction_key AS id,
       kd.identity_key,
       kd.bank_key,
	   'ECHECKSELECT' AS product_id,
       adt.merchant_acc_no AS merchant_id,
       upper(adt.merchant_acc_no) AS merchant_id_key,
       adt.merchant_customer_id AS merchant_user_id,
       adt.merchant_trans_id,
       hex(adt.trans_id) trans_id,
       adt.trans_type_id,
       adt.trans_amt * 100 AS trans_amount,
       adt.trans_amt * 100 AS trans_amount_converted,
       adt.currency_code AS trans_currency,
       CASE WHEN adt.tax_state IS NOT NULL THEN adt.tax_state ELSE 'NJ' END AS trans_state,
       adt.country AS trans_country_code,
       adt.trans_datetime,
       adt.sub_trans_type_id AS trans_subtype_id,
       adt.trans_status_id,
       adt.ext_reference_no AS trans_ext_reference_no,
       adt.channel AS trans_channel,
       COALESCE(mn.error_code, adt.trans_detail) AS trans_error_code,
       adt.originator_ip AS trans_originator_ip,
       adt.payee AS trans_payee,
       initcap(adt.first_name) || ' ' || initcap(adt.last_name) AS cust_full_name,
       initcap(adt.first_name) AS cust_first_name,
       initcap(adt.last_name) AS cust_last_name,
       CASE WHEN adt.line1 IS NOT NULL THEN adt.line1 || ', ' ELSE '' END || CASE WHEN adt.line2 IS NOT NULL THEN adt.line2 || ', ' ELSE '' end || CASE WHEN adt.city IS NOT NULL THEN adt.city || ', ' ELSE '' END || adt.postal_code AS cust_address,
       adt.postal_code AS cust_zip_code,
       adt.country AS cust_country_code,
       'INDIVIDUAL' AS cust_identity_type,
       NULLIF(NULLIF(adt.hph, 'nullnull'), '') AS cust_phone_number,
       adt.dob AS cust_date_of_birth,
       NULLIF(adt.ssn, '') AS cust_government_id,
       CASE WHEN NULLIF(adt.ssn, '') IS NULL THEN NULL ELSE 'SSN' END AS cust_government_id_type,
       NULL AS cust_government_id_state,
       adt.drv_lic_no AS cust_driving_license,
       adt.drv_lic_state AS cust_driving_license_state,
       bl.log_date AS login_datetime,
       bl.customer_account_balance AS login_balance,
       bl.customer_account_balance AS login_balance_converted,
       bl.customer_account_balance_currency AS login_balance_currency,
       adt.fi_routing AS bank_fi_routing,
       adt.fi_acc AS bank_fi_acc,
       adt.fi_acc_type_id AS bank_fi_acc_type
FROM db2admin.ach_detail_trans adt
INNER JOIN key_data kd ON kd.trans_id = adt.trans_id
LEFT JOIN db2admin.big_log bl ON bl.trans_id = adt.trans_id
LEFT JOIN db2admin.merchant_ntfy mn ON mn.trans_id = adt.trans_id 
WHERE bl.echeck_type_id = 'V' AND adt.trans_type_id = '1' AND adt.merchant_customer_id IS NOT NULL AND adt.dob IS NOT NULL 
	AND (
		(adt.trans_datetime BETWEEN (TIMESTAMP('2021-04-19 00:23:00.000') + 3 HOURS) 
							AND (TIMESTAMP('2021-04-19 02:57:47.000') + 3 HOURS))
		OR (adt.trans_datetime BETWEEN (TIMESTAMP('2021-04-19 18:20:00.000') + 3 HOURS) 
							AND (TIMESTAMP('2021-04-21 09:36:00.000') + 3 HOURS))
		)	
ORDER BY adt.trans_datetime
;


-- Payout
-- 7. Credit Check All Rules Query
WITH 
key_data(trans_id, transaction_key, identity_key, bank_key, merchant_customer_key) AS 
(
  SELECT x.trans_id,
         'ECHECKSELECT|' || hex(x.trans_id) AS transaction_key,
  		 'US|' || upper(x.last_name) || '|'  || upper(x.postal_code) || '|' || x.dob identity_key,
         CASE WHEN nullif(x.fi_routing, '') IS NOT NULL AND x.fi_acc NOT LIKE '%**%' THEN 'US|' || x.fi_acc || '|' || x.fi_routing || '|' || CASE WHEN x.fi_acc_type_id = 'C' THEN 'CHECKING' WHEN x.fi_acc_type_id = 'S' THEN 'SAVINGS' ELSE '' END ELSE NULL END AS bank_key,
         'ECHECKSELECT|' || upper(x.merchant_acc_no) || '|' || upper(x.merchant_customer_id) AS merchant_customer_key
  FROM db2admin.ach_detail_trans x
)
SELECT 
	   'credit_check_all_rules' AS event_type,
       CAST(date(adt.trans_datetime - current timezone) AS varchar(32)) || 'T' || replace(CAST(time(adt.trans_datetime - current timezone) AS varchar(32)), '.', ':') || '.' || CAST(microsecond(adt.trans_datetime) AS varchar(8)) || 'Z' AS $timestamp, -- UTC
       kd.transaction_key AS id,
       kd.identity_key,
       kd.bank_key,
       kd.merchant_customer_key,
	   'ECHECKSELECT' AS product_id,
       adt.merchant_acc_no AS merchant_id,
       upper(adt.merchant_acc_no) AS merchant_id_key,
       adt.merchant_customer_id AS merchant_user_id,
       adt.merchant_trans_id,
       hex(adt.trans_id) trans_id,
       adt.trans_type_id,
       adt.trans_amt * 100 AS trans_amount,
       adt.trans_amt * 100 AS trans_amount_converted,
       adt.currency_code AS trans_currency,
       CASE WHEN adt.tax_state IS NOT NULL THEN adt.tax_state ELSE 'NJ' END AS trans_state,
       adt.country AS trans_country_code,
       adt.sub_trans_type_id AS trans_subtype_id,
       adt.trans_status_id,
       adt.ext_reference_no AS trans_ext_reference_no,
       adt.channel AS trans_channel,
       adt.trans_detail AS trans_error_code,
       adt.originator_ip AS trans_originator_ip,
       adt.payee AS trans_payee,
       initcap(adt.first_name) || ' ' || initcap(adt.last_name) AS cust_full_name,
       initcap(adt.first_name) AS cust_first_name,
       initcap(adt.last_name) AS cust_last_name,
       CASE WHEN adt.line1 IS NOT NULL THEN adt.line1 || ', ' ELSE '' END || CASE WHEN adt.line2 IS NOT NULL THEN adt.line2 || ', ' ELSE '' end || CASE WHEN adt.city IS NOT NULL THEN adt.city || ', ' ELSE '' END || adt.postal_code AS cust_address,
       adt.postal_code AS cust_zip_code,
       adt.country AS cust_country_code,
       'INDIVIDUAL' AS cust_identity_type,
       NULLIF(NULLIF(adt.hph, 'nullnull'), '') AS cust_phone_number,
       CAST(adt.dob AS varchar(32)) || 'T00:00:00.000000Z' AS cust_date_of_birth, -- UTC
       NULLIF(adt.ssn, '') AS cust_government_id,
       CASE WHEN NULLIF(adt.ssn, '') IS NULL THEN NULL ELSE 'SSN' END AS cust_government_id_type,
       NULL AS cust_government_id_state,
       adt.drv_lic_no AS cust_driving_license,
       adt.drv_lic_state AS cust_driving_license_state,
       adt.fi_routing AS bank_fi_routing,
       adt.fi_acc AS bank_fi_acc,
       adt.fi_acc_type_id AS bank_fi_acc_type
FROM db2admin.ach_detail_trans adt
INNER JOIN key_data kd ON kd.trans_id = adt.trans_id
WHERE adt.trans_type_id = '3' AND adt.merchant_customer_id IS NOT NULL AND adt.dob IS NOT NULL 
-- Payout
-- 7. Credit Check All Rules Query
WITH 
key_data(trans_id, transaction_key, identity_key, bank_key, merchant_customer_key) AS 
(
  SELECT x.trans_id,
         'ECHECKSELECT|' || hex(x.trans_id) AS transaction_key,
  		 'US|' || upper(x.last_name) || '|'  || upper(x.postal_code) || '|' || x.dob identity_key,
         CASE WHEN nullif(x.fi_routing, '') IS NOT NULL AND x.fi_acc NOT LIKE '%**%' THEN 'US|' || x.fi_acc || '|' || x.fi_routing || '|' || CASE WHEN x.fi_acc_type_id = 'C' THEN 'CHECKING' WHEN x.fi_acc_type_id = 'S' THEN 'SAVINGS' ELSE '' END ELSE NULL END AS bank_key,
         'ECHECKSELECT|' || upper(x.merchant_acc_no) || '|' || upper(x.merchant_customer_id) AS merchant_customer_key
  FROM db2admin.ach_detail_trans x
)
SELECT 
	   'credit_check_all_rules' AS event_type,
       CAST(date(adt.trans_datetime - current timezone) AS varchar(32)) || 'T' || replace(CAST(time(adt.trans_datetime - current timezone) AS varchar(32)), '.', ':') || '.' || CAST(microsecond(adt.trans_datetime) AS varchar(8)) || 'Z' AS $timestamp, -- UTC
       kd.transaction_key AS id,
       kd.identity_key,
       kd.bank_key,
       kd.merchant_customer_key,
	   'ECHECKSELECT' AS product_id,
       adt.merchant_acc_no AS merchant_id,
       upper(adt.merchant_acc_no) AS merchant_id_key,
       adt.merchant_customer_id AS merchant_user_id,
       adt.merchant_trans_id,
       hex(adt.trans_id) trans_id,
       adt.trans_type_id,
       adt.trans_amt * 100 AS trans_amount,
       adt.trans_amt * 100 AS trans_amount_converted,
       adt.currency_code AS trans_currency,
       CASE WHEN adt.tax_state IS NOT NULL THEN adt.tax_state ELSE 'NJ' END AS trans_state,
       adt.country AS trans_country_code,
       adt.sub_trans_type_id AS trans_subtype_id,
       adt.trans_status_id,
       adt.ext_reference_no AS trans_ext_reference_no,
       adt.channel AS trans_channel,
       adt.trans_detail AS trans_error_code,
       adt.originator_ip AS trans_originator_ip,
       adt.payee AS trans_payee,
       initcap(adt.first_name) || ' ' || initcap(adt.last_name) AS cust_full_name,
       initcap(adt.first_name) AS cust_first_name,
       initcap(adt.last_name) AS cust_last_name,
       CASE WHEN adt.line1 IS NOT NULL THEN adt.line1 || ', ' ELSE '' END || CASE WHEN adt.line2 IS NOT NULL THEN adt.line2 || ', ' ELSE '' end || CASE WHEN adt.city IS NOT NULL THEN adt.city || ', ' ELSE '' END || adt.postal_code AS cust_address,
       adt.postal_code AS cust_zip_code,
       adt.country AS cust_country_code,
       'INDIVIDUAL' AS cust_identity_type,
       NULLIF(NULLIF(adt.hph, 'nullnull'), '') AS cust_phone_number,
       CAST(adt.dob AS varchar(32)) || 'T00:00:00.000000Z' AS cust_date_of_birth, -- UTC
       NULLIF(adt.ssn, '') AS cust_government_id,
       CASE WHEN NULLIF(adt.ssn, '') IS NULL THEN NULL ELSE 'SSN' END AS cust_government_id_type,
       NULL AS cust_government_id_state,
       adt.drv_lic_no AS cust_driving_license,
       adt.drv_lic_state AS cust_driving_license_state,
       adt.fi_routing AS bank_fi_routing,
       adt.fi_acc AS bank_fi_acc,
       adt.fi_acc_type_id AS bank_fi_acc_type
FROM db2admin.ach_detail_trans adt
INNER JOIN key_data kd ON kd.trans_id = adt.trans_id
WHERE adt.trans_type_id = '3' AND adt.merchant_customer_id IS NOT NULL AND adt.dob IS NOT NULL 
	AND (
		(adt.trans_datetime BETWEEN (TIMESTAMP('2021-04-19 00:23:00.000') + 3 HOURS) 
							AND (TIMESTAMP('2021-04-19 02:57:47.000') + 3 HOURS))
		OR (adt.trans_datetime BETWEEN (TIMESTAMP('2021-04-19 18:20:00.000') + 3 HOURS) 
							AND (TIMESTAMP('2021-04-21 09:36:00.000') + 3 HOURS))
		)
ORDER BY adt.trans_datetime
;



-- Return
-- 8. Representment Transaction NSF1
WITH 
key_data(trans_id, transaction_key, identity_key, bank_key, merchant_customer_key) AS 
(
  SELECT x.trans_id,
         'ECHECKSELECT|' || hex(x.trans_id) AS transaction_key,
  		 'US|' || upper(x.last_name) || '|'  || upper(x.postal_code) || '|' || x.dob identity_key,
         CASE WHEN nullif(x.fi_routing, '') IS NOT NULL AND x.fi_acc NOT LIKE '%**%' THEN 'US|' || x.fi_acc || '|' || x.fi_routing || '|' || CASE WHEN x.fi_acc_type_id = 'C' THEN 'CHECKING' WHEN x.fi_acc_type_id = 'S' THEN 'SAVINGS' ELSE '' END ELSE NULL END AS bank_key,
         'ECHECKSELECT|' || upper(x.merchant_acc_no) || '|' || upper(x.merchant_customer_id) AS merchant_customer_key
  FROM db2admin.ach_detail_trans x
),
return_data(parent_id, return_report_datetime, return_code, return_next_action, return_next_action_date, settlement_code) AS
(
  SELECT AT.parent_id, ast.report_datetime, ast.return_code, ast.next_action, ast.reinit_date, ast.settlement_code
  FROM db2admin.ach_trans at 
  INNER JOIN db2admin.ach_settlement_trans ast ON at.trans_id = ast.parent_id
)
SELECT 
	   'representment' AS event_type,
       CAST(date(nsf1.return_report_datetime - current timezone) AS varchar(32)) || 'T' || replace(CAST(time(nsf1.return_report_datetime - current timezone) AS varchar(32)), '.', ':') || '.' || CAST(microsecond(nsf1.return_report_datetime) AS varchar(8)) || 'Z' AS timestamp, -- UTC
	   kd.transaction_key as id,
	   kd.identity_key,
	   kd.bank_key,
	   'ECHECKSELECT' AS product_id,
	   adt.merchant_acc_no AS merchant_id,
       upper(adt.merchant_acc_no) AS merchant_id_key,
	   adt.merchant_customer_id AS merchant_user_id,
	   adt.merchant_trans_id AS merchant_trans_id,
       hex(adt.trans_id) AS trans_id,
       CAST(date(nsf1.return_report_datetime - current timezone) AS varchar(32)) || 'T' || replace(CAST(time(nsf1.return_report_datetime - current timezone) AS varchar(32)), '.', ':') || '.' || CAST(microsecond(nsf1.return_report_datetime) AS varchar(8)) || 'Z' AS return_nsf1_date, -- UTC
       nsf1.return_next_action,
       CAST(nsf1.return_next_action_date AS varchar(32)) || 'T00:00:00.000000Z' AS return_next_action_date -- UTC
FROM db2admin.ach_detail_trans adt
INNER JOIN key_data kd ON kd.trans_id = adt.trans_id
INNER JOIN return_data nsf1 ON adt.trans_id = nsf1.parent_id AND nsf1.return_next_action = 'R' AND nsf1.return_code = 'R01' AND nsf1.settlement_code = 4
WHERE adt.trans_type_id = '1' AND adt.merchant_customer_id IS NOT NULL AND adt.dob IS NOT NULL 
	AND (
		(adt.trans_datetime BETWEEN (TIMESTAMP('2021-04-19 00:23:00.000') + 3 HOURS) 
							AND (TIMESTAMP('2021-04-19 02:57:47.000') + 3 HOURS))
		OR (adt.trans_datetime BETWEEN (TIMESTAMP('2021-04-19 18:20:00.000') + 3 HOURS) 
							AND (TIMESTAMP('2021-04-21 09:36:00.000') + 3 HOURS))
		)
ORDER BY adt.trans_datetime
;

-- Return
-- 9. Representment Transaction NSF2
WITH 
key_data(trans_id, transaction_key, identity_key, bank_key, merchant_customer_key) AS 
(
  SELECT x.trans_id,
         'ECHECKSELECT|' || hex(x.trans_id) AS transaction_key,
  		 'US|' || upper(x.last_name) || '|'  || upper(x.postal_code) || '|' || x.dob identity_key,
         CASE WHEN nullif(x.fi_routing, '') IS NOT NULL AND x.fi_acc NOT LIKE '%**%' THEN 'US|' || x.fi_acc || '|' || x.fi_routing || '|' || CASE WHEN x.fi_acc_type_id = 'C' THEN 'CHECKING' WHEN x.fi_acc_type_id = 'S' THEN 'SAVINGS' ELSE '' END ELSE NULL END AS bank_key,
         'ECHECKSELECT|' || upper(x.merchant_acc_no) || '|' || upper(x.merchant_customer_id) AS merchant_customer_key
  FROM db2admin.ach_detail_trans x
),
return_data(parent_id, return_report_datetime, return_code, return_next_action, return_next_action_date, settlement_code) AS
(
  SELECT AT.parent_id, ast.report_datetime, ast.return_code, ast.next_action, ast.reinit_date, ast.settlement_code
  FROM db2admin.ach_trans at 
  INNER JOIN db2admin.ach_settlement_trans ast ON at.trans_id = ast.parent_id
)
SELECT 
	   'representment' AS event_type,
       CAST(date(nsf2.return_report_datetime - current timezone) AS varchar(32)) || 'T' || replace(CAST(time(nsf2.return_report_datetime - current timezone) AS varchar(32)), '.', ':') || '.' || CAST(microsecond(nsf2.return_report_datetime) AS varchar(8)) || 'Z' AS timestamp, -- UTC
	   kd.transaction_key AS id,
	   kd.identity_key,
	   kd.bank_key,
	   'ECHECKSELECT' AS product_id,
	   adt.merchant_acc_no AS merchant_id,
       upper(adt.merchant_acc_no) AS merchant_id_key,
	   adt.merchant_customer_id AS merchant_user_id,
	   adt.merchant_trans_id AS merchant_trans_id,
       hex(adt.trans_id) AS trans_id,
       CAST(date(nsf2.return_report_datetime - current timezone) AS varchar(32)) || 'T' || replace(CAST(time(nsf2.return_report_datetime - current timezone) AS varchar(32)), '.', ':') || '.' || CAST(microsecond(nsf2.return_report_datetime) AS varchar(8)) || 'Z' AS return_nsf2_date, -- UTC
       nsf2.return_next_action,
       CAST(nsf2.return_next_action_date AS varchar(32)) || 'T00:00:00.000000Z' AS return_next_action_date -- UTC
FROM db2admin.ach_detail_trans adt
INNER JOIN key_data kd ON kd.trans_id = adt.trans_id
INNER JOIN return_data nsf2 ON adt.trans_id = nsf2.parent_id AND nsf2.return_next_action = 'R' AND nsf2.return_code = 'R01' AND nsf2.settlement_code = 5
WHERE adt.trans_type_id = '1' AND adt.merchant_customer_id IS NOT NULL AND adt.dob IS NOT NULL 
	AND (
		(adt.trans_datetime BETWEEN (TIMESTAMP('2021-04-19 00:23:00.000') + 3 HOURS) 
							AND (TIMESTAMP('2021-04-19 02:57:47.000') + 3 HOURS))
		OR (adt.trans_datetime BETWEEN (TIMESTAMP('2021-04-19 18:20:00.000') + 3 HOURS) 
							AND (TIMESTAMP('2021-04-21 09:36:00.000') + 3 HOURS))
		)
ORDER BY adt.trans_datetime
;

-- Return
-- 10. Transaction Final Return
WITH 
key_data(trans_id, transaction_key, identity_key, bank_key, merchant_customer_key) AS 
(
  SELECT x.trans_id,
         'ECHECKSELECT|' || hex(x.trans_id) AS transaction_key,
  		 'US|' || upper(x.last_name) || '|'  || upper(x.postal_code) || '|' || x.dob identity_key,
         CASE WHEN nullif(x.fi_routing, '') IS NOT NULL AND x.fi_acc NOT LIKE '%**%' THEN 'US|' || x.fi_acc || '|' || x.fi_routing || '|' || CASE WHEN x.fi_acc_type_id = 'C' THEN 'CHECKING' WHEN x.fi_acc_type_id = 'S' THEN 'SAVINGS' ELSE '' END ELSE NULL END AS bank_key,
         'ECHECKSELECT|' || upper(x.merchant_acc_no) || '|' || upper(x.merchant_customer_id) AS merchant_customer_key
  FROM db2admin.ach_detail_trans x
),
return_data(parent_id, return_report_datetime, return_code, return_next_action, return_next_action_date, settlement_code) AS
(
  SELECT AT.parent_id, ast.report_datetime, ast.return_code, ast.next_action, ast.reinit_date, ast.settlement_code
  FROM db2admin.ach_trans at 
  INNER JOIN db2admin.ach_settlement_trans ast ON at.trans_id = ast.parent_id
)
SELECT 
	   'return' AS event_type,
       CAST(date(final_return.return_report_datetime - current timezone) AS varchar(32)) || 'T' || replace(CAST(time(final_return.return_report_datetime - current timezone) AS varchar(32)), '.', ':') || '.' || CAST(microsecond(final_return.return_report_datetime) AS varchar(8)) || 'Z' AS timestamp, -- UTC
	   kd.transaction_key AS id,
	   kd.identity_key,
	   kd.bank_key,
	   'ECHECKSELECT' AS product_id,
	   adt.merchant_acc_no AS merchant_id,
       upper(adt.merchant_acc_no) AS merchant_id_key,
	   adt.merchant_customer_id AS merchant_user_id,
	   adt.merchant_trans_id AS merchant_trans_id,
       hex(adt.trans_id) trans_id,
       CAST(date(final_return.return_report_datetime - current timezone) AS varchar(32)) || 'T' || replace(CAST(time(final_return.return_report_datetime - current timezone) AS varchar(32)), '.', ':') || '.' || CAST(microsecond(final_return.return_report_datetime) AS varchar(8)) || 'Z' AS return_datetime, -- UTC
       final_return.return_code,
       adt.trans_amt * 100 AS return_amount,
       adt.trans_amt * 100 AS return_amount_converted,
       adt.currency_code AS return_currency,
       final_return.return_next_action,
       NULL AS return_next_action_date -- UTC
FROM db2admin.ach_detail_trans adt
INNER JOIN key_data kd ON kd.trans_id = adt.trans_id
INNER JOIN return_data final_return ON adt.trans_id = final_return.parent_id AND final_return.return_next_action = 'D'
WHERE adt.trans_type_id = '1' AND adt.merchant_customer_id IS NOT NULL AND adt.dob IS NOT NULL 
	AND (
		(adt.trans_datetime BETWEEN (TIMESTAMP('2021-04-19 00:23:00.000') + 3 HOURS) 
							AND (TIMESTAMP('2021-04-19 02:57:47.000') + 3 HOURS))
		OR (adt.trans_datetime BETWEEN (TIMESTAMP('2021-04-19 18:20:00.000') + 3 HOURS) 
							AND (TIMESTAMP('2021-04-21 09:36:00.000') + 3 HOURS))
		)
ORDER BY adt.trans_datetime
;

